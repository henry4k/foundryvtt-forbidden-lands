import { CounterBase, required } from './counter-base'

export class CounterModifier extends CounterBase {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            minValue: 0,
            maxValue: required,
            styleFn: required
        })
    }

    /**
     * @param {number} minValue
     * @param {number} maxValue
     * @param {function} styleFn fn(entryElement, value, counterModifier)
     */
    constructor(o) {
        super(o)

        if(o.value < o.minValue || o.value > o.maxValue)
            o.value = o.minValue
    }

    /** @override */
    //render() {
    //    super.render()

    //    const o = this.options
    //    const element = o.element

    //    element.innerHTML = ''
    //    //for(const child of element.childNodes.values())
    //    //    child.remove()

    //    const minValue = o.minValue
    //    const boxCount = o.maxValue - minValue + 1
    //    for(let i = 0; i <= boxCount-1; i++) {
    //        const entryElement = document.createElement('i')
    //        o.styleFn(entryElement, minValue+i, this)
    //        element.appendChild(entryElement)
    //    }
    //}

    /** @override */
    render() {
        super.render()

        const o = this.options
        const element = o.element

        element.innerHTML = ''
        //for(const child of element.childNodes.values())
        //    child.remove()

        //const minValue = o.minValue
        //const boxCount = o.maxValue - minValue + 1
        //for(let i = 0; i <= boxCount-1; i++) {
        //    const entryElement = document.createElement('i')
        //    o.styleFn(entryElement, minValue+i, this)
        //    element.appendChild(entryElement)
        //}

        const value = o.value
        if(value > 0) {
            for(let i = 1; i <= value; i++) {
                const entryElement = document.createElement('i')
                o.styleFn(entryElement, i, this)
                element.appendChild(entryElement)
            }
        } else if(value < 0) {
            for(let i = -1; i >= value; i--) {
                const entryElement = document.createElement('i')
                o.styleFn(entryElement, i, this)
                element.appendChild(entryElement)
            }
        } else {
            const entryElement = document.createElement('i')
            o.styleFn(entryElement, 0, this)
            element.appendChild(entryElement)
        }
    }

    /** @override */
    update(changes) {
        super.update(changes)
        const o = this.options
        o.value = Math.max(o.minValue, Math.min(o.maxValue, o.value))
    }

    /** @override */
    _onClick(event) {
        event.preventDefault()
        const o = this.options
        if(event.type === 'click') {
            if(o.value < o.maxValue)
                return this.commit({value: o.value+1})
        } else {
            if(o.value > o.minValue)
                return this.commit({value: o.value-1})
        }
        this.feedback({})
    }
}
