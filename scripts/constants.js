export const systemRoot = 'systems/forbidden-lands-henry4k'

export const diceChain = [6,8,10,12]
export const diceChainIndex = (function() {
    const index = {}
    diceChain.forEach((dice, i) => {
        index[dice] = i
    })
    return index
})()
export const getDieWithOffset = (die, chainOffset) => {
    let index = diceChainIndex[die.toString()]
    if(index === undefined)
        return null
    index += chainOffset
    if(index < 0 || index >= diceChain.length)
        return null
    return diceChain[index]
}

export const skills = {
    might: {},
    endurance: {},
    melee: {},
    crafting: {},
    stealth: {},
    sleightOfHand: {},
    move: {},
    marksmanship: {},
    scouting: {},
    lore: {},
    survival: {},
    insight: {},
    manipulation: {},
    performance: {},
    healing: {},
    animalHandling: {}
}
for(const [name, skill] of Object.entries(skills)) {
    skill.name = name
}
Hooks.once('setup', function() {
    for(const [name, skill] of Object.entries(skills)) {
        skill.label = game.i18n.localize(`FL.${name.capitalize()}`)
    }
})

export const attributes = {
    strength: {
        skills: ['might', 'endurance', 'melee', 'crafting']
    },
    agility: {
        skills: ['stealth', 'sleightOfHand', 'move', 'marksmanship']
    },
    wits: {
        skills: ['scouting', 'lore', 'survival', 'insight']
    },
    empathy: {
        skills: ['manipulation', 'performance', 'healing', 'animalHandling']
    }
}
for(const [name, attribute] of Object.entries(attributes)) {
    attribute.name = name
    attribute.skills = attribute.skills.map(skillName => {
        const skill = skills[skillName]
        skill.attribute = attribute
        return skill
    })
}
Hooks.once('setup', function() {
    for(const [name, attribute] of Object.entries(attributes)) {
        attribute.label = game.i18n.localize(`FL.${name.capitalize()}`)
    }
})

export const resources = {
    food: {},
    water: {},
    arrows: {},
    torches: {}
}
export const resourceList = ['food', 'water', 'arrows', 'torches'].map(
    name => resources[name])
for(const [name, resource] of Object.entries(resources)) {
    resource.name = name
    resource.imageLineart = `${systemRoot}/ui/resources/${name}.svg`
    resource.imageColor   = `${systemRoot}/ui/resources/${name}-color.png`
}
Hooks.once('setup', function() {
    for(const resource of Object.values(resources)) {
        resource.label = game.i18n.localize(`FL.${resource.name.capitalize()}`)
    }
})

export const itemTypes = {
    weapon:    {isGear: true},
    armor:     {isGear: true},
    equipment: {isGear: true},
    spell: {},
    talent: {},
    criticalInjury: {},
    disease: {},
    strongholdFunction: {},
    strongholdHireling: {}
}
for(const [name, type] of Object.entries(itemTypes)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(itemTypes)) {
        type.label = game.i18n.localize(`FL.${name.capitalize()}`)
    }
})
export const itemTypeNames = Object.keys(itemTypes)

export const rangeTypes = {
    varies: {},
    personal: {},
    arm: {},
    near: {},
    short: {},
    long: {},
    distant: {}
}
for(const [name, type] of Object.entries(rangeTypes)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(rangeTypes)) {
        type.label = game.i18n.localize(`FL.Range${name.capitalize()}`)
        type.description = game.i18n.localize(`FL.Range${name.capitalize()}Description`)
    }
})

export const durationTypes = {
    varies:     {},
    immediate:  {},
    oneRound:   {},
    oneTurn:    {},
    quarterDay: {inQuarterDays:     1},
    oneDay:     {inQuarterDays:   1*2},
    twoDays:    {inQuarterDays:   2*2},
    oneWeek:    {inQuarterDays:   7*2},
    twoWeeks:   {inQuarterDays:  14*2},
    oneMonth:   {inQuarterDays: 4*7*2}
}
for(const [name, type] of Object.entries(durationTypes)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(durationTypes)) {
        type.label = game.i18n.localize(`FL.Duration${name.capitalize()}`)
    }
})

export const timePeriods = {
    weeks: {},
    days: {},
    hours: {},
    minutes: {},
    turns: {}
}
for(const [name, type] of Object.entries(timePeriods)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(timePeriods)) {
        type.labelSingular = game.i18n.localize(`FL.Time${name.capitalize()}Singular`)
        type.labelPlural   = game.i18n.localize(`FL.Time${name.capitalize()}Plural`)
    }
})

export const dayQuarters = [
    {name: 'morning', iconRotation: -90},
    {name: 'day',     iconRotation:   0},
    {name: 'evening', iconRotation:  90},
    {name: 'night',   iconRotation: 180}
]
Hooks.once('setup', function() {
    for(const quarter of dayQuarters)
        quarter.label = game.i18n.localize(`FL.Quarter${quarter.name.capitalize()}`)
})

export const intervalList = [
    {value:   0, label: '—'},
    {value:   1, times: 1, timePeriod: timePeriods.days},
    {value:   2, times: 2, timePeriod: timePeriods.days},
    {value:   3, times: 3, timePeriod: timePeriods.days},
    {value:   4, times: 4, timePeriod: timePeriods.days},
    {value: 7*4, times: 1, timePeriod: timePeriods.weeks},
]
const _intervalByValue = Object.fromEntries(intervalList.map(
    interval => [`${interval.value}`, interval]))
export const intervalByValue = value => _intervalByValue[value]
Hooks.once('setup', function() {
    for(const interval of intervalList) {
        if(!interval.label)
            interval.label = `${interval.times}&MediumSpace;×&MediumSpace;/${interval.timePeriod.labelSingular}`
    }
})

export const weightClasses = {
    none:   {value: 0, label: '—'},
    tiny:   {value: 0},
    light:  {value: 0.5},
    normal: {value: 1},
    heavy:  {value: 2}
}
for(const [name, type] of Object.entries(weightClasses)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(weightClasses)) {
        if(!type.label)
            type.label = game.i18n.localize(`FL.Weight${name.capitalize()}`)
    }
})
export const getWeightInfoByName = name => {
    const weightClass = weightClasses[name]
    if(weightClass)
        return weightClass
    const heavy = weightClasses.heavy
    return {
        value: parseFloat(name),
        name: name,
        label: `${heavy.label} (${name})`
    }
}

export const talentTypes = {
    kin:        {resolve: actorData => actorData.kin},
    profession: {resolve: actorData => actorData.profession},
    general:    {resolve: () => ''}
}
for(const [name, type] of Object.entries(talentTypes)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(talentTypes)) {
        type.label = game.i18n.localize(`FL.TalentType${name.capitalize()}`)
    }
})

export const spellTypes = {
    spell: {},
    powerWord: {},
    ritual: {}
}
for(const [name, type] of Object.entries(spellTypes)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(spellTypes)) {
        type.label = game.i18n.localize(`FL.${name.capitalize()}`)
        type.description = game.i18n.localize(`FL.${name.capitalize()}Description`)
    }
})

export const features = {
    blunt: {},
    edged: {},
    hook: {},
    parrying: {},
    pointed: {}
}
for(const [name, type] of Object.entries(features)) {
    type.name = name
}
Hooks.once('setup', function() {
    for(const [name, type] of Object.entries(features)) {
        type.label = game.i18n.localize(`FL.Feature${name.capitalize()}`)
    }
})

let namedNumbers = ['zero', 'one', 'two']
Hooks.once('setup', function() {
    namedNumbers = namedNumbers.map(name => game.i18n.localize(`FL.${name.capitalize()}`))
})
export const numberName = value => namedNumbers[value] || value.toString()

export const constantsData = {
    attributes: attributes,
    rangeTypes: rangeTypes,
    durationTypes: durationTypes,
    timePeriods: timePeriods,
    intervalList: intervalList,
    weightClasses: weightClasses,
    talentTypes: talentTypes,
    spellTypes: spellTypes,
    features: features
}
