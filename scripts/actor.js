import { stepProduction } from './production'
import { FLItem } from './item'

export const actorTypes = ['character', 'stronghold']

export class FLActor extends Actor {

    /** @override */
    getRollData() {
        const data = super.getRollData()
        switch(this.data.type) {
            case 'character':
                this._getCharacterRollData(data)
                break
        }
    }


    /* ---- CHARACTER ---- */

    _getCharacterRollData(data) {
        // Re-map all attribute values onto the base roll data
        for(const [name, attribute] of Object.entries(data.attributes)) {
            data[name] = attribute.value
        }

        // Re-map all skill values onto the base roll data
        for(const [name, skill] of Object.entries(data.skills)) {
            data[name] = skill.value
        }
    }

    async addSpellsFromWorld(talentName) {
        const predicate = item => {
            if(item.type !== 'spell')
                return false
            const spellTalent = item.data.data.talent
            return spellTalent === talentName ||
                   spellTalent === ''
        }

        const ownedSpellsByName = Object.fromEntries(this.items
            .filter(predicate)
            .map(item => [item.name, item]))

        const spellsInWorld = await FLItem.findItemsInWorld(predicate)

        const newSpells = spellsInWorld.filter(spell =>
            !(spell.name in ownedSpellsByName))

        const newSpellData = newSpells.map(spell => {
            const data = duplicate(spell.data)
            delete data._id
            return data
        })

        return this.createOwnedItem(newSpellData)
    }

    async applyDamage({attributes={}, items={}, willpower=false}) {
        const promises = []

        // Update actor itself:

        const actorUpdate = {}
        let actorUpdateEmpty = true

        const actorData = this.data.data

        for(const [attribute, damage] of Object.entries(attributes)) {
            const oldValue = actorData.attributes[attribute].value
            const newValue = Math.max(oldValue - damage, 0)
            if(oldValue == newValue)
                continue
            actorUpdate[`data.attributes.${attribute}.value`] = newValue
            actorUpdateEmpty = false
        }

        if(willpower) {
            const totalAttributeDamage = Object.entries(attributes) .reduce((previous, [_attribute, damage]) => previous+damage, 0)
            actorUpdate['data.willpower.value'] =
                actorData.willpower.value + totalAttributeDamage
        }

        if(!actorUpdateEmpty)
            promises.push(this.update(actorUpdate))


        // Update affected items:
        for(const [itemId, damage] of Object.entries(items)) {
            const item = this.getOwnedItem(itemId)
            // if(!item)
            // TODO: Signal error to user
            const oldValue = item.data.data.bonus.value
            const newValue = Math.max(oldValue - damage, 0)
            if(oldValue == newValue)
                continue
            promises.push(this.updateOwnedItem({
                _id: item.id,
                'data.bonus.value': newValue
            }))
        }

        return Promise.all(promises)
    }


    /* ---- STRONGHOLD ---- */

    getRawMaterialItem(name) {
        return this.items.find(
            item => item.name === name &&
                    item.type === 'equipment')
    }

    /*
    async _createRawMaterialItem(name, amount=0) {
        return this.createOwnedItem({
            name: name,
            type: 'equipment',
            data: {amount: amount}
        })
    }

    async getOrCreateRawMaterialItem(name) {
        const item = this.findRawMaterialItem(name)
        if(item)
            return item
        const itemData = await this._createRawMaterialItem(name)
        return this.getOwnedItem(itemData._id)
    }
    */

    async setRawMaterialAmount(materialAmounts) {
        const createData = []
        const updateData = []
        for(const [material, newAmount] of Object.entries(materialAmounts)) {
            const item = this.getRawMaterialItem(material)
            if(item) {
                if(item.data.data.amount !== newAmount)
                    updateData.push({
                        _id: item._id,
                        'data.amount': newAmount
                    })
            } else {
                if(newAmount !== 0)
                    createData.push({
                        name: material,
                        type: 'equipment',
                        data: {amount: newAmount}
                    })
            }
        }
        if(createData.length > 0)
            await this.createOwnedItem(createData)
        if(updateData.length > 0)
            await this.updateOwnedItem(updateData)
    }

    async stepContinousProduction(quarterDays=1) {
        let quarterDay = this.data.data.quarterDay
        for(; quarterDays > 0; quarterDays--) {
            const producers = this.items.filter(item =>
                item.type === 'strongholdFunction' &&
                item.isProducing(quarterDay))
            this.stepProduction(producers)
            quarterDay++
        }
        this.update({'data.quarterDay': quarterDay})
    }

    async stepProduction(producers) {
        stepProduction(this, producers)
    }
}
