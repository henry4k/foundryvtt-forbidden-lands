import { activateNumberInputs, activateToggleButtons } from './utils'
import { constantsData } from './constants'
import { itemTypes, itemTypeNames } from './constants'
import { rollFromButton } from './roll/roll'

export const sortItems = items =>
    items.sort((a, b) => (a.sort || 0) - (b.sort || 0))

export class FLActorSheet extends ActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            dragDrop: [{dragSelector: ".item .item-name", dropSelector: null}]
        })
    }


    /** @override */
    getData() {
        const sheetData = super.getData()

        sheetData.constants = constantsData

        const itemsByType = {}
        for(const type of itemTypeNames)
            itemsByType[type] = []
        for(const item of sheetData.items)
            itemsByType[item.type].push(item)
        for(const items of Object.values(itemsByType))
            sortItems(items)
        sheetData.itemsByType = itemsByType

        return sheetData
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html)

        html = html[0] // substitute jQuery-Object for the real HTMLElement

        // Everything below here is only needed if the sheet is editable
        if(!this.options.editable)
            return

        activateNumberInputs(this, html)
        activateToggleButtons(this, html)

        const rollButtonTitle = game.i18n.localize('FL.Roll')
        for(const element of html.querySelectorAll('.roll-button')) {
            element.title = rollButtonTitle
            element.addEventListener('click', this._clickRollButton.bind(this))
        }

        for(const element of html.querySelectorAll('.item-create'))
            element.addEventListener('click', this._onItemCreate.bind(this))

        for(const element of html.querySelectorAll('.item-grid > [data-item-id]'))
            this._activateItem(element)
    }

    _clickRollButton(event) {
        rollFromButton(event, this.actor)
    }

    ///** @override */
    //_onDragStart(event) {
    //    // use itemId of the closest element instead of the current one
    //    event.currentTarget.closest('[data-item-id]')
    //}

    _activateItem(itemElement) {
        const itemId = itemElement.dataset.itemId
        const item = this.actor.getOwnedItem(itemId)

        const itemNameElement = itemElement.querySelector('.item-name')

        itemNameElement.dataset.itemId = itemId
        // As we use a grid layout we have no real item container.
        // Thus the name element is used as a drag handle.

        itemNameElement.addEventListener('click', event => {
            event.preventDefault()
            item.sheet.render(true)
        })

        activateNumberInputs(this, itemElement, item, '.item-amount > [contenteditable]')

        if(itemTypes[item.type].isGear) {
            const equipToggle = itemElement.querySelector('.item-equip')
            if(equipToggle)
                equipToggle.addEventListener('click', event => {
                    event.preventDefault()
                    item.equip(!item.data.data.equipped)
                })
        }

        itemElement.querySelector('.item-delete').addEventListener('click', event => {
            event.preventDefault()
            this.actor.deleteOwnedItem(itemId)
            $(itemElement).slideUp(200, () => this.render(false))
        })
    }

    _onItemCreate(event) {
        event.preventDefault()
        const button = event.currentTarget
        const type = button.dataset.type
        return this._createAndEditItem(type, duplicate(button.dataset))
    }

    _createAndEditItem(type, data) {
        const itemData = {
            name: game.i18n.format('FL.NewItem', {type: itemTypes[type].label}),
            type: type,
            data: data || {}
        }
        delete itemData.data.type
        return this.actor.createOwnedItem(itemData, {renderSheet: true})
    }

    _editItem(event, item) {
        item.sheet.render(true)
    }

    _deleteItem(event, item, itemElement) {
        this.actor.deleteOwnedItem(item._id)
        $(itemElement).slideUp(200, () => this.render(false))
    }
}
