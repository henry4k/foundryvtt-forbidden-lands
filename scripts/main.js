// Import Modules
import { FLActor } from './actor'
import { FLItem } from './item'
import { FLItemSheet } from './item-sheet'
import { FLCharacterSheet } from './character-sheet'
import { FLStrongholdSheet } from './stronghold-sheet'
import { systemRoot, itemTypeNames } from './constants'
import './roll/core'
import './roll/roll'
import './roll/dialog'
import './roll/dice-so-nice'


/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once('init', async function() {
    console.log('Initializing Forbidden Lands')

    /**
     * Set an initiative formula for the system
     * @type {String}
     */
    CONFIG.Combat.initiative = {
        formula: '1d20',
        decimals: 2
    }

    // Define custom Entity classes
    CONFIG.Actor.entityClass = FLActor
    CONFIG.Item.entityClass = FLItem

    // Register sheet application classes
    Actors.unregisterSheet('core', ActorSheet)
    Actors.registerSheet('forbidden-lands', FLCharacterSheet, {
        types: ['character'],
        makeDefault: true
    })
    Actors.registerSheet('forbidden-lands', FLStrongholdSheet, {
        types: ['stronghold']
    })
    Items.unregisterSheet('core', ItemSheet)
    Items.registerSheet('forbidden-lands', FLItemSheet, {
        types: itemTypeNames,
        makeDefault: true
    })

    CONFIG.TinyMCE.content_css.push(systemRoot+'/style.css')

    // Register system settings
    //game.settings.register('forbidden-lands', 'macroShorthand', {
    //    name: 'Shortened Macro Syntax',
    //    hint: 'Enable a shortened macro syntax which allows referencing attributes directly, for example @str instead of @attributes.str.value. Disable this setting if you need the ability to reference the full attribute model, for example @attributes.str.label.',
    //    scope: 'world',
    //    type: Boolean,
    //    default: true,
    //    config: true
    //})
})
