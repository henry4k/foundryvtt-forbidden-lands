let valueHint
Hooks.once('setup', function() {
    valueHint = game.i18n.localize('FL.CounterBarModifyValueHint')
})

export const required = Symbol('required')

export class CounterBase {
    static get defaultOptions() {
        return {
            element: required,
            value: required,
            editable: true,
            commitFn: null
        }
    }

    /**
     * @param {HTMLElement} element
     * @param {number} value
     * @param {function} commitFn fn(options); pass null to prevent editing
     * @param {boolean} editable
     */
    constructor(o) {
        for(const [option, defaultValue] of Object.entries(this.constructor.defaultOptions)) {
            if(o[option] === undefined) {
                if(defaultValue === required)
                    throw new Error(`Required parameter ${option} is missing`)
                else
                    o[option] = defaultValue
            }
        }
        this.options = o

        if(!o.commitFn)
            o.commitFn = changes => {
                this.update(changes)
                this.render()
            }

        if(o.editable) {
            const element = o.element
            element.addEventListener('click',       this._onClick.bind(this))
            element.addEventListener('contextmenu', this._onClick.bind(this))
        }

        this.render()
    }

    render() {
        const o = this.options
        if(o.editable) {
            const element = o.element
            element.title = valueHint
            element.classList.add('editable')
        }
    }

    update(changes) {
        this.feedback(changes)
        mergeObject(this.options, changes)
    }

    commit(changes) {
        this.update(changes)
        this.options.commitFn(this.options)
    }

    feedback(changes) {
        if(Object.keys(changes).length > 0)
            AudioHelper.play({src: CONFIG.sounds.notification})
        else
            AudioHelper.play({src: CONFIG.sounds.lock})
    }

    _onClick(event) {
        event.preventDefault()
        const value = this.options.value
        if(event.type === 'click')
            return this.commit({value: value+1})
        else
            return this.commit({value: value-1})
    }
}
