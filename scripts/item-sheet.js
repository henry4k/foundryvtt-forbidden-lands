import { registerTemplate, activateInlineLists } from './utils'
import { constantsData, itemTypeNames, features } from './constants'
import { activateCounterBars } from './counter-bar'
import { activateDieSelections } from './counter-item'


const itemSheetTemplate = registerTemplate('item-sheet')
registerTemplate('gear-details')
registerTemplate('gear-bonus')
const detailsTemplates = {}
itemTypeNames.forEach(type => detailsTemplates[type] = registerTemplate(type+'-details'))

const onClick = (rootElement, selector, callback) =>
    rootElement.querySelectorAll(selector).forEach(element =>
        element.addEventListener('click', callback))

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class FLItemSheet extends ItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['forbidden-lands', 'sheet', 'item'],
            template: itemSheetTemplate,
            width: 520,
            height: 480,
            tabs: [{navSelector: 'nav.tabs', contentSelector: '.sheet-body', initial: 'description'}]
        })
    }

    /** @override */
    getData() {
        const data = super.getData()
        data.detailsTemplate = () => detailsTemplates[this.item.type]
        data.constants = constantsData

        if(data.data.features) {
            const selectedFeatures = data.data.features
            data.unselectedFeatures = Object.values(features)
                .filter(feature => !selectedFeatures.includes(feature.name))
                .sort((a,b) => a.label.localeCompare(b.label))
        }

        switch(this.item.type) {
            case 'strongholdFunction':
                this._getStrongholdFunctionData(data)
                break
            case 'spell':
                this._getSpellData(data)
                break
        }

        return data
    }

    _getStrongholdFunctionData(data) {
        if(!this.actor)
            return
        const names = {}
        this.actor.items.filter(item => item.type === 'equipment') // TODO TEST
                        .forEach(item => names[item.name] = true)
        data.knownMaterials = Object.keys(names).sort()
    }

    _getSpellData(data) {
        const names = {}
        if(this.actor)
            this.actor.items.filter(item => item.type === 'spell')
                            .forEach(item => names[item.data.data.talent] = true)
        game.items.filter(item => item.visible && item.type === 'spell')
                  .forEach(item => names[item.data.data.talent] = true)
        data.magicTalentNames = Object.keys(names).sort()
    }

    /** @override */
    setPosition(options={}) {
        const position = super.setPosition(options)
        const sheetBody = this.element.find('.sheet-body')
        const bodyHeight = position.height - 192
        sheetBody.css('height', bodyHeight)
        return position
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html)

        html = html[0] // substitute jQuery-Object for the real HTMLElement

        if(this.item.type === 'criticalInjury')
            this._activateCriticalInjury(html)

        activateCounterBars(this, html)
        activateDieSelections(this, html)

        // Everything below here is only needed if the sheet is editable
        if(!this.options.editable)
            return

        //activateValueButtons(this, html)

        activateInlineLists(html, '.feature-list',
            this.item.addFeature.bind(this.item),
            this.item.removeFeature.bind(this.item))

        activateInlineLists(html, '.consumed-material-list',
            this.item.addConsumedMaterial.bind(this.item),
            this.item.removeConsumedMaterial.bind(this.item))

        activateInlineLists(html, '.produced-material-list',
            this.item.addProducedMaterial.bind(this.item),
            this.item.removeProducedMaterial.bind(this.item))
    }

    _activateCriticalInjury(html) {
        if(!this.options.editable)
            return

        onClick(html, '.time-limit-roll', event => {
            event.preventDefault()
            this.item.rollTimeLimit()
        })
        onClick(html, '.time-limit-reset', event => {
            event.preventDefault()
            this.item.resetTimeLimit()
        })
        onClick(html, '.healing-time-roll', event => {
            event.preventDefault()
            this.item.rollHealingTime()
        })
        onClick(html, '.healing-time-reset', event => {
            event.preventDefault()
            this.item.resetHealingTime()
        })
    }
}
