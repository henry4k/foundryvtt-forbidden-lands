import { FLSkillDialogModule } from './dialog-skill'
import { FLModifierDialogModule } from './dialog-modifier'
import { FLGearDialogModule } from './dialog-gear'
import { FLRoll } from './core'
import { rollDialog } from './dialog'

const userCanPushRoll = message => message.can(game.user, 'update')

const userCanTakePushDamage = message => {
    const actor = message.constructor.getSpeakerActor(message.data.speaker)
    return actor
        ? actor.can(game.user, 'update')
        : false
}

const getTargetActor = () => {
    // TODO: Move to utils.js
    // TODO: Maybe add option to select actor manually (using a dialog or so)
    const token = canvas.ready
               && (canvas.tokens.controlled.length === 1)
                  ? canvas.tokens.controlled[0]
                  : null
    return token
        ? token.actor
        : game.user.character
}

const messageFromHtml = element => {
    element = element[0] // substitute jQuery-Object for the real HTMLElement
    const message = game.messages.get(element.dataset.messageId)
    return message
}

const contextMenuEntries = {
    pushRoll: {
        label: 'Push Roll',
        icon: '<i class="fas fa-dice"></i>',
        condition: message => userCanPushRoll(message)
                           && message.roll.isPushable(),
        callback: message => {
            const roll = message.roll
            roll.push()
            const messageData = message.data
            roll.toMessage({
                speaker: messageData.speaker,
                flavor: messageData.flavor,
                flags: messageData.flags
            }, {
                rollMode: messageData.rollMode || game.settings.get('core', 'rollMode')
            })
            // TODO: Make clear that the roll has been pushed!
        }
    },
    takePushDamage: {
        label: 'Take Push Damage',
        icon: '<i class="fas fa-skull"></i>',
        condition: message => userCanTakePushDamage(message)
                           && message.roll.canApplyPushDamage(),
        callback: message => {
            const actor = ChatMessage.getSpeakerActor(message.data.speaker)
            message.roll.applyPushDamage(actor)
        }
    },
    parryRoll: {
        label: 'Parry Attack',
        icon: '<i class="fas fa-dice"></i>',
        condition: message => message.roll.total > 0,
        callback: message => {
            return commonRoll({
                actor: getTargetActor(),
                rollType: 'defense',
                rollTypeOptions: {
                    editableSkill: true, // REALLY?
                    skill: 'melee'
                    // TODO
                }
            })
        }
    },
    dodgeRoll: {
        label: 'Dodge Attack',
        icon: '<i class="fas fa-dice"></i>',
        condition: message => message.roll.total > 0,
        callback: message => {
            return commonRoll({
                actor: getTargetActor(),
                rollType: 'defense',
                rollTypeOptions: {
                    // TODO
                },
                speaker: message.data.speaker
            })
        }
    },
    armorRoll: {
        label: 'Armor Roll',
        icon: '<i class="fas fa-dice"></i>',
        condition: message => message.roll.total > 0, // TODO
        callback: message => {
            return commonRoll({
                actor: getTargetActor(),
                rollType: 'armor',
                rollTypeOptions: {
                    // TODO
                },
                speaker: message.data.speaker
            })
        }
    },
    takeAttackDamage: {
        label: 'Take Attack Damage',
        icon: '<i class="fas fa-skull"></i>',
        condition: message => message.roll.total > 0,
        callback: message => {
            const roll = message.roll
            const damage = roll.total // TODO: Add item damage
            const actor = getTargetActor()
            actor.applyDamage({
                attributes: {
                    strength: damage
                }
            })
        }
    }
}

Hooks.on('getChatLogEntryContext', (_html, entryOptions) => {
    entryOptions.push.apply(entryOptions, Object.entries(contextMenuEntries).map(([name, entry]) => {
        const condition = entry.condition
        const callback = entry.callback
        return {
            name: entry.label,
            icon: entry.icon,
            condition: element => {
                const message = messageFromHtml(element)
                const roll = message.roll
                return roll instanceof FLRoll
                    && getRollTypeDescFromRoll(roll).contextMenuEntries.includes(name)
                    && condition(message)
            },
            callback: element => {
                const message = messageFromHtml(element)
                return callback(message)
            }
        }
    }))
})

export const rollTypes = {}

rollTypes.default = {
    title: 'SKILL ROLL',
    dialogModules: o => {
        return [
            {class: FLSkillDialogModule, editable: o.editableSkill, skill: o.skill},
            {class: FLModifierDialogModule},
            {class: FLGearDialogModule, items: o.items || []}
        ]
    },
    flavor: roll => {
        // crafts something with Hammer, Forge, and Carpenter Tools
        // ...
        return '???'
    },
    chatTemplateData: (roll, chatOptions) => {
        return {}
    },
    contextMenuEntries: [
        'pushRoll',
        'takePushDamage'
    ]
}

rollTypes.attack = {
    title: 'ATTACK ROLL',
    dialogModules: o => {
        return [
            {class: FLSkillDialogModule, editable: o.editableSkill, skill: o.skill},
            {class: FLModifierDialogModule},
            {class: FLGearDialogModule, items: o.items || []}
        ]
    },
    flavor: roll => {
        // attacks with Short Sword
        // slashes with Short Sword
        // stabs with Dagger
        return `??? ${roll.options.type} roll ???`
    },
    chatTemplateData: (roll, chatOptions) => {
        const isPrivate = chatOptions.isPrivate
        return {
            resultDescription: isPrivate ? null : `??? ${roll.total} ???`
        }
    },
    contextMenuEntries: [
        'pushRoll',
        'takePushDamage',
        'parryRoll',
        'dodgeRoll',
        'armorRoll',
        'takeAttackDamage'
    ]
}

rollTypes.defense = {
    title: 'DEFENSE ROLL',
    dialogModules: o => {
        return [
            {class: FLSkillDialogModule, editable: o.editableSkill, skill: o.skill},
            {class: FLModifierDialogModule},
            {class: FLGearDialogModule, items: o.items || []}
        ]
    },
    flavor: roll => {
        // dodges an attack
        // parries an attack with Small Shield
        return `??? ${roll.options.type} roll ???`
    },
    chatTemplateData: (roll, chatOptions) => {
        const isPrivate = chatOptions.isPrivate
        return {
            resultDescription: isPrivate ? null : `??? ${roll.total} ???`
        }
    },
    contextMenuEntries: [
        'pushRoll',
        'takePushDamage',
        'armorRoll',
        'takeAttackDamage'
    ]
}

rollTypes.armor = {
    title: 'ARMOR ROLL',
    dialogModules: o => {
        return [
            {class: FLGearDialogModule, items: o.items || []}
        ]
    },
    flavor: roll => {
        // Leather Armor and Leather Helmet absorb damage
        return `??? ${roll.options.type} roll ???`
    },
    chatTemplateData: (roll, chatOptions) => {
        const isPrivate = chatOptions.isPrivate
        return {
            resultDescription: isPrivate ? null : `??? ${roll.total} ???`
        }
    },
    contextMenuEntries: [
        'takeAttackDamage'
    ]
}

const defaultRollType = rollTypes.default

export const getRollTypeDescFromName = rollType => {
    return rollTypes[rollType] || defaultRollType
}

export const getRollTypeDescFromRoll = roll => {
    return 'type' in roll.options
        ? rollTypes[roll.options.type]
        : defaultRollType
}

Hooks.on('forbidden-lands.renderRoll', (roll, chatOptions, chatData) => {
    mergeObject(chatData, getRollTypeDescFromRoll(roll).chatTemplateData(roll, chatOptions))
})

/**
 * @param {FLActor} actor
 * @param {string|null} rollType
 * @param {Object|null} rollTypeOptions
 * @param {string|null} flavor
 * @param {Object|null} speaker
 *
 * See rollDialog for more options.
 *
 * @return {Promise} A Promise which resolves once the roll workflow has completed
 */
export const commonRoll = async options => {
    const {actor,
           rollType,
           rollTypeOptions={},
           flavor,
           speaker} = options

    const rollTypeDesc = getRollTypeDescFromName(rollType)

    options.modules = rollTypeDesc.dialogModules(rollTypeOptions)
    if(!('title' in options))
        options.title = `${actor.name}: ${rollTypeDesc.title}`

    const {roll, rollMode} = await rollDialog(options)
    roll.options.type = rollType

    const message = roll.toMessage({
        speaker: speaker || ChatMessage.getSpeaker({actor: actor}),
        flavor: flavor || rollTypeDesc.flavor(roll)
    }, {
        rollMode: rollMode
    })

    return message
}
