import { registerTemplate } from '../utils'
import { skills } from '../constants'
import { FLRollDialogModule } from './dialog'


const template = registerTemplate('roll-dialog-skill')
export class FLSkillDialogModule extends FLRollDialogModule {
    constructor(options) {
        super(options)
        this.editable = options.editable
        this.skill = options.skill
    }

    /** @override */
    getDiceTerms() {
        const {attribute, skill} = this._getAttributeAndSkill()
        return [
            {
                dieType: 'base',
                value: attribute.value,
                options: {
                    flavor: attribute.label,
                    attribute: attribute.name
                }
            },
            {
                dieType: 'skill',
                value: skill.value,
                options: {
                    flavor: skill.label,
                    skill: skill.name
                }
            }
        ]
    }

    /** @override */
    update(data) {
        this.skill = data.skill
    }

    /** @override */
    template() {
        return template
    }

    /** @override */
    getData() {
        const r = super.getData()
        mergeObject(r, this._getAttributeAndSkill())
        r.editable = this.editable
        return r
    }

    _getAttributeAndSkill() {
        const skill = this.skill
        if(skill) {
            const skillDesc = skills[skill]
            const attributeDesc = skillDesc.attribute
            const actorData = this.dialog.actor.data.data
            const attributeData = actorData.attributes[skillDesc.attribute.name]
            const skillData = actorData.skills[skill]
            return {
                attribute: {
                    name: attributeDesc.name,
                    label: attributeDesc.label,
                    value: attributeData.value,
                    max: attributeData.max
                },
                skill: {
                    name: skill,
                    label: skillDesc.label,
                    value: skillData.value
                }
            }
        } else {
            return {
                attribute: {
                    name: '',
                    label: '',
                    value: 0,
                    max: 0
                },
                skill: {
                    name: '',
                    label: '',
                    value: 0
                }
            }
        }
    }
}
