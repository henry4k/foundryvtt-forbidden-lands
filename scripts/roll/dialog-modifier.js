import { registerTemplate, numberFromHash } from '../utils'
import { CounterModifier } from '../counter-modifier'
import { FLRollDialogModule } from './dialog'

const modifierDieStyleFn = (element, value, counterModifier) => {
    const o = counterModifier.options
    const activeValue = o.value

    const visible = (activeValue < 0 && value < 0 && value >= activeValue) ||
                    (activeValue > 0 && value > 0 && value <= activeValue)

    const cl = element.classList
    cl.add('die')
    cl.add('d6')
    cl.add(`${o.dieType}-pool`)
    if(!visible)
        cl.add('placeholder')
    else
        element.style.transform = `rotate(${numberFromHash(-3, 3, o.id+value)}deg)`
    if(value < 0)
        cl.add('negative')
    if(value !== 0)
        element.innerText = value.signedString()
}

const template = registerTemplate('roll-dialog-modifier')
export class FLModifierDialogModule extends FLRollDialogModule {
    constructor(options) {
        super(options)
        this.dieType = options.dieType || 'skill'
        this.value = options.value || 0
    }

    /** @override */
    getDiceTerms() {
        return [
            {
                dieType: this.dieType,
                value: this.value,
                modifier: true,
                options: {
                    flavor: game.i18n.localize('FL.Modifier')
                }
            }
        ]
    }

    /** @override */
    template() {
        return template
    }

    /** @override */
    activateListeners(html) {
        this.counterModifier = new CounterModifier({
            id: 'modifier',
            element: html.querySelector('[data-name=modifierCounter]'),
            value: this.value,
            minValue: -5,
            maxValue: +5,
            editable: true,
            styleFn: modifierDieStyleFn,
            dieType: this.dieType,
            commitFn: changes => {
                this.value = changes.value
                this.counterModifier.update(changes)
                this.counterModifier.render()
            }
        })
    }
}
