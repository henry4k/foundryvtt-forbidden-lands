import { registerTemplate, numberFromHash } from '../utils'
import { CounterBar } from '../counter-bar'
import { createDieSelection } from '../counter-item'
import { FLItem } from '../item'
import { FLRollDialogModule } from './dialog'

const dieStyleFn = (element, value, state, counterBar) => {
    const o = counterBar.options
    const cl = element.classList
    cl.add('die')
    cl.add('d6')
    cl.add(`${o.dieType}-pool`)
    switch(state) {
        case 'off':
            cl.add('placeholder')
            break
        case 'placeholder':
            cl.add('placeholder')
            cl.add('crossed-out')
            break
        case 'hidden':
            cl.add('hidden')
            break
    }
    if(state === 'on')
        element.style.transform = `rotate(${numberFromHash(-3, 3, o.id+value)}deg)`
}

const template = registerTemplate('roll-dialog-gear')
export class FLGearDialogModule extends FLRollDialogModule {
    constructor(options) {
        super(options)

        this.customGearBonus = options.customGearBonus || {
            value: 0,
            artifactDie: 0
        }

        this.items = []
        this.itemsById = {}
        for(const item of options.items || [])
            this.addItem(item)
    }

    /** @override */
    close(options) {
        for(const item of this.items)
            delete item.apps[this.appId]
    }

    addItem(item) {
        if(item.id in this.itemsById)
            return
        this.itemsById[item.id] = item
        this.items.push(item)
        item.apps[this.appId] = this
    }

    removeItem(item) {
        const id = item.id
        if(!(id in this.itemsById))
            return
        delete this.itemsById[id]
        const index = this.items.findIndex(item => item.id === id)
        this.items.splice(index, 1)
        delete item.apps[this.appId]
    }

    /** @override */
    getDiceTerms() {
        const r = this.items.map(item => {
            return {
                dieType: 'gear',
                value: item.data.data.bonus.value,
                options: {
                    flavor: item.name,
                    item: item.id
                }
            }
        })
        r.push({
            dieType: 'gear',
            value: this.customGearBonus.value
        })
        r.push({
            dieType: 'artifact',
            value: this.customGearBonus.artifactDie
        })
        return r
    }

    /** @override */
    update(data) {
        this.customGearBonus = data.customGearBonus
    }

    /** @override */
    template() {
        return template
    }

    /** @override */
    getData() {
        const r = super.getData()
        r.customGearBonus = this.customGearBonus
        r.items = this.items
        return r
    }

    /** @override */
    activateListeners(html) {
        for(const itemElement of html.querySelectorAll('.item')) {
            const id = itemElement.dataset.itemId
            const item = this.itemsById[id]
            const nameElement = itemElement.querySelector('.item-name')
            nameElement.addEventListener('click', event => {
                event.preventDefault()
                item.sheet.render(true)
            })
            const deleteElement = itemElement.querySelector('.item-delete')
            deleteElement.addEventListener('click', event => {
                event.preventDefault()
                this.removeItem(item)
                this.render()
            })
        }

        const customGearBonus = this.customGearBonus

        this.customGearBonusCounter = this._activateDiceCounterBar({
            id: 'customGearBonus',
            element: html.querySelector('[data-name=customGearBonus]'),
            //softLimit: 5,
            hardLimit: 5,
            omit: 'off',
            editable: true,
            dieType: 'gear',
            commitFn: changes => {
                customGearBonus.value = changes.value
                this.customGearBonusCounter.update(changes)
                this.customGearBonusCounter.render()
            }
        })

        const dieTypes = [0,8,10,12]
        const artifactDieSelection = createDieSelection(
            customGearBonus.artifactDie,
            dieTypes,
            o => {
                customGearBonus.artifactDie = o.mappedValue
                artifactDieSelection.render()
                // no need to call submit
            },
            {element: html.querySelector('[data-name=customArtifactDie]'),
             mappedValue: customGearBonus.artifactDie,
             editable: true}
        )
    }

    _activateDiceCounterBar(o) {
        mergeObject(o, {
            styleFn: dieStyleFn,
            placeholder: true,
            omit: 'hidden'
        }, {overwrite: false})
        const dataset = o.element.dataset
        if(dataset.value)
            o.value = parseInt(dataset.value)
        if(dataset.softLimit)
            o.softLimit = parseInt(dataset.softLimit)
        return new CounterBar(o)
    }

    async _onDropItem(data) {
        let item
        if(data.actorId) {
            const actor = game.actors.get(data.actorId)
            item = actor.getOwnedItem(data.data._id)
        } else {
            item = await FLItem.fromDropData(data)
        }

        this.addItem(item)
        this.dialog.render()
    }

    /** @override */
    onDrop(data) {
        if(data.type !== 'Item')
            return false
        this._onDropItem(data)
        return true
    }
}
