import { rollDialog } from './dialog'
import { commonRoll as commonRoll_ } from './roll-types'

/**
 * @param {FLActor} actor
 * @param {string|null} flavor
 *
 * See rollDialog for more options.
 *
 * @return {Promise} A Promise which resolves once the roll workflow has completed
 */
export const customRoll = async options => {
    const {roll, rollMode} = await rollDialog(options)

    const message = roll.toMessage({
        speaker: ChatMessage.getSpeaker({actor: options.actor}),
        flavor: options.flavor
    }, {
        rollMode: rollMode
    })

    return message
}

export const commonRoll = commonRoll_

export const rollFromButton = (event, actor) => {
    event.preventDefault()
    const element = event.currentTarget
    const d = element.dataset
    return commonRoll({
        event: event,
        actor: actor,
        rollType: (d.type && d.type !== '')
            ? d.type
            : undefined,
        rollTypeOptions: {
            skill: (d.skill && d.skill !== '')
                ? d.skill
                : undefined,
            editableSkill: d.editableSkill === 'true',
            items: (d.item && d.item !== '')
                ? [actor.getOwnedItem(d.item)]
                : []
        }
    })
}
