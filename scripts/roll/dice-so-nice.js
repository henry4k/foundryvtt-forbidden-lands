/* ---- Dice So Nice support ---- */

let _createMessageFromRoll = (roll, data) => roll.toMessage(roll, data)

/**
 * Returns a promise which resolves as soon as DSN handled the roll.
 *
 * Without DSN this does nothing special.
 */
export const createMessageFromRoll = (roll, data) => _createMessageFromRoll(roll, data)

Hooks.once('diceSoNiceReady', dice3d => {
    const messageResolvers = {}

    _createMessageFromRoll = async (roll, data) => {
        const message = await roll.toMessage(data)
        return new Promise(resolve => {
            messageResolvers[message.id] = resolve
        })
    }

    Hooks.on('diceSoNiceRollComplete', messageId => {
        const resolve = messageResolvers[messageId]
        if(resolve) {
            delete messageResolvers[messageId]
            resolve()
        }
    })

    class FLBaseDie extends Die {}
    FLBaseDie.NAME = 'base'
    FLBaseDie.DENOMINATION = 'b'

    class FLSkillDie extends Die {}
    FLSkillDie.NAME = 'skill'
    FLSkillDie.DENOMINATION = 's'

    class FLNegativeDie extends Die {}
    FLNegativeDie.NAME = 'negative'
    FLNegativeDie.DENOMINATION = 'n'

    class FLGearDie extends Die {}
    FLGearDie.NAME = 'gear'
    FLGearDie.DENOMINATION = 'g'

    class FLArtifactDie8 extends Die {}
    FLArtifactDie8.NAME = 'artifact8'
    FLArtifactDie8.DENOMINATION = 'a8'

    class FLArtifactDie10 extends Die {}
    FLArtifactDie10.NAME = 'artifact10'
    FLArtifactDie10.DENOMINATION = 'a10'

    class FLArtifactDie12 extends Die {}
    FLArtifactDie12.NAME = 'artifact12'
    FLArtifactDie12.DENOMINATION = 'a12'

    dice3d.addSystem({id: 'forbidden-lands', name: 'Forbidden Lands'}, true)

    dice3d.addColorset({
        name: 'forbidden-lands-base',
        description: 'Forbidden Lands Base',
        category: 'Forbidden Lands',
        foreground: '#000000',
        background: '#FFFFFF',
        outline: null,
        edge: null
    }, 'no')
    dice3d.addColorset({
        name: 'forbidden-lands-skill',
        description: 'Forbidden Lands Skill',
        category: 'Forbidden Lands',
        foreground: '#FFFFFF',
        background: '#821D23',
        outline: null,
        edge: null
    }, 'no')
    dice3d.addColorset({
        name: 'forbidden-lands-gear',
        description: 'Forbidden Lands Gear',
        category: 'Forbidden Lands',
        foreground: '#FFFFFF',
        background: '#000000',
        outline: null,
        edge: null
    }, 'no')
    dice3d.addColorset({
        name: 'forbidden-lands-negative',
        description: 'Forbidden Lands Negative Skill',
        category: 'Forbidden Lands',
        foreground: 'hsl(0deg, 100%, 80%)',
        background: 'darkblue',
        outline: null,
        edge: null
    }, 'no')

    for(const name of ['base', 'skill', 'gear', 'negative']) {
        const hasBanes = name == 'base' || name == 'gear'
        dice3d.addDicePreset({
            system: 'forbidden-lands',
            type: `d${name[0]}`,
            font: 'Forbidden Lands Icons',
            fontScale: 2,
            labels: [hasBanes ? '@' : '', '', '', '', '', '$'],
            colorset: `forbidden-lands-${name}`
        }, 'd6')
    }
    dice3d.addDicePreset({
        system: 'forbidden-lands',
        type: 'da8',
        font: 'Forbidden Lands Icons',
        labels: ['', '', '', '', '', '$', '$', '$\n$'],
        //colorset: `forbidden-lands-gear`
    }, 'd8')
    dice3d.addDicePreset({
        system: 'forbidden-lands',
        type: 'da10',
        font: 'Forbidden Lands Icons',
        labels: ['', '', '', '', '', '$', '$', '$\n$', '$\n$', '$\n$ $'],
        //colorset: `forbidden-lands-gear`
    }, 'd10')
    dice3d.addDicePreset({
        system: 'forbidden-lands',
        type: 'da12',
        font: 'Forbidden Lands Icons',
        labels: ['', '', '', '', '', '$', '$', '$\n$', '$\n$', '$\n$ $', '$\n$ $', '$$\n$$'],
        //colorset: `forbidden-lands-gear`
    }, 'd12')

    const getDieClass = diceTerm => {
        switch(diceTerm.options.flType) {
            case 'base':
                return FLBaseDie
            case 'skill':
                return diceTerm.negative
                    ? FLNegativeDie
                    : FLSkillDie
            case 'gear':
                return FLGearDie
            case 'artifact':
                switch(diceTerm.faces) {
                    case 8:
                        return FLArtifactDie8
                    case 10:
                        return FLArtifactDie10
                    case 12:
                        return FLArtifactDie12
                }
        }
    }

    const filterPushedDice = roll => {
        const pushCount = roll.pushCount
        if(pushCount == 0)
            return null //roll
        roll = Roll.fromJSON(JSON.stringify(roll))
        for(const term of roll.dice)
            term.results = term.results.filter(r => r.pushNumber == pushCount)
        return roll
    }

    Hooks.on('diceSoNiceRollStart', (_messageId, context) => {
        let originalRoll = context.roll
        if(!originalRoll.dice.some(diceTerm => diceTerm.options.flType))
            return

        const isPushed = originalRoll.pushCount >= 1
        const clonedRoll = isPushed ? filterPushedDice(originalRoll)
                                    : Roll.fromJSON(JSON.stringify(originalRoll))

        for(const term of clonedRoll.dice) {
            if(term.options.flType)
                term.constructor = getDieClass(term)
        }
        context.roll = clonedRoll
    })
})
