import { registerTemplate, random } from '../utils'

const getSuccessCount = dieResult => Math.max(0, Math.floor((dieResult-4)/2))

const dieTypeByModifier = {
    base: 'base',
    b:    'base',
    skill: 'skill',
    s:     'skill',
    gear: 'gear',
    g:    'gear',
    artifact: 'artifact',
    a:        'artifact'
}

Hooks.once('init', async () => {
    Object.assign(Die.MODIFIERS, {
        base:     'flTypeModifier',
        b:        'flTypeModifier',
        skill:    'flTypeModifier',
        s:        'flTypeModifier',
        gear:     'flTypeModifier',
        g:        'flTypeModifier',
        artifact: 'flTypeModifier',
        a:        'flTypeModifier'
    })

    Object.assign(Die.prototype, {
        canHaveBanes: function() {
            switch(this.options.flType) {
                case 'base':
                case 'gear':
                    return true
                default:
                    return false
            }
        },

        flTypeModifier: function(modifier) {
            const dieType = dieTypeByModifier[modifier]
            this.options.flType = dieType
            for(const result of this.results)
                result.count = getSuccessCount(result.result)
        },

        push: function(pushNumber = 1) {
            const canHaveBanes = this.canHaveBanes()
            const results = this.results
            const length = results.length
            for(let i = 0; i < length; i++) {
                const result = results[i]
                const isBane = canHaveBanes && result.result === 1
                const isSuccess = result.result >= 6
                const isPushable = !isBane && !isSuccess
                if(isPushable) {
                    const newResult = this.roll()
                    results.pop()
                    newResult.count = getSuccessCount(newResult.result)
                    newResult.rerolled = true
                    newResult.pushNumber = pushNumber
                    results[i] = newResult
                }
            }
        }
    })

    Object.defineProperties(Die.prototype, {
        banes: {
            configurable: true,
            get: function() {
                if(!this._evaluated)
                    return null
                if(!this.canHaveBanes())
                    return 0
                return this.results.reduce((t, r) => {
                    if(!r.active)
                        return t
                    else
                        return t + (r.result == 1 ? 1 : 0)
                }, 0)
            }
        }
    })
})

export class FLRoll extends Roll {
    constructor(formula, data={}, options={}) {
        super(formula, data)
        this.options = options
    }

    /** @override */
    clone() {
        return new this.constructor(this._formula, this.data, this.options)
    }

    /** @override */
    reroll() {
        return new this.constructor(this.formula, this.data, this.options).roll()
    }

    /** @override */
    evaluate(options={}) {
        super.evaluate(options)
        this._markNegativeSkillDice()
        return this
    }

    /** @override */
    toJSON() {
        const json = super.toJSON()
        json.options = this.options
        //if('_pushCount' in this)
        //    json.pushCount = this._pushCount
        return json
    }

    /** @override */
    static fromData(data) {
        const roll = super.fromData(data)
        roll.options = data.options || {}
        //if('pushCount' in data)
        //    roll._pushCount = data.pushCount
        roll._markNegativeSkillDice()
        return roll
    }

    _markNegativeSkillDice() {
        const terms = this.terms
        let negative = false
        for(let i = 0; i < terms.length; i++) {
            const term = terms[i]
            if(term === '-') {
                negative = true
            } else {
                if(term instanceof Die && term.options.flType)
                    term.negative = negative
                negative = false
            }
        }
    }

    isPureFlRoll() {
        return this.terms.every(t => t === '+' ||
                                     t === '-' ||
                                     (t instanceof Die && t.options.flType))
    }

    _reevaluate() {
        // Code copied from evaluate method

        // Evaluate any remaining terms
        this.results = this.terms.map(term => term.evaluate ? term.total : term)

        // Safely evaluate the final total
        const total = this._safeEval(this.results.join(' '))
        if(!Number.isNumeric(total))
            throw new Error(game.i18n.format('DICE.ErrorNonNumeric', {formula: this.formula}))

        // Store final outputs
        this._total = total
    }

    get pushCount() {
        //return this._pushCount || 0
        return this.options.pushCount || 0
    }

    push() {
        const pushCount = this.pushCount + 1
        this.options.pushCount = pushCount
        for(const term of this.dice)
            if(term instanceof Die && term.options.flType)
                term.push(pushCount)
        this._reevaluate()
    }

    isPushed() {
        return this.pushCount > 0
        //return this.dice.some(d => d instanceof Die &&
        //                           d.options.flType &&
        //                           d.results.some(r => r.options.rerolled))
    }

    isPushable() {
        return this.dice.some(term => {
            if(!(term instanceof Die))
                return false
            const canHaveBanes = term.canHaveBanes()
            return term.results.some(result => {
                const isBane = canHaveBanes && result.result === 1
                const successCount = getSuccessCount(result.result)
                const isSuccess = successCount >= 1
                return !isBane && !isSuccess // is pushable?
            })
        })
    }

    hasBanes() {
        return this.dice.some(term => {
            if(!(term instanceof Die))
                return false
            if(!term.canHaveBanes())
                return false
            return term.results.some(result => result.result === 1) // is bane?
        })
    }

    /** @override */
    async render(chatOptions = {}) {
        if(!this.isPureFlRoll())
            return super.render(chatOptions)

        chatOptions = mergeObject({
            user: game.user._id,
            flavor: null,
            template: this.constructor.FL_CHAT_TEMPLATE,
            blind: false
        }, chatOptions)
        const isPrivate = chatOptions.isPrivate

        // Execute the roll, if needed
        if(!this._rolled)
            this.roll()

        // Define chat data
        const chatData = {
            user: chatOptions.user,
            flavor: isPrivate ? null : chatOptions.flavor
        }

        if(!isPrivate) {
            const pushCount = this.pushCount
            chatData.dice = this.dice.map(term => {
                const canHaveBanes = term.canHaveBanes()
                const options = term.options
                const dieType = options.flType
                return {
                    flavor: options.flavor,
                    results: term.results.map(result => {
                        const isBane = canHaveBanes && result.result === 1
                        const successCount = getSuccessCount(result.result)
                        const isSuccess = successCount >= 1
                        const isPushable = !isBane && !isSuccess
                        const isPushed = result.pushNumber == pushCount
                        return {
                            classes: [
                                'd'+term.faces,
                                dieType+'-pool',
                                isPushed ? 'pushed' : null,
                                isPushable ? 'pushable' : null,
                                isBane ? 'bane' : null,
                                isSuccess ? 'success-'+successCount : null,
                                term.negative ? 'negative' : null
                            ].filter(c => c).join(' '),
                            randomRotation: random(-3, 3),
                            animationDelay: random(0, 100),
                        }
                    })
                }
            })
        }

        Hooks.callAll('forbidden-lands.renderRoll', this, chatOptions, chatData)

        // Render the roll display template
        return renderTemplate(chatOptions.template, chatData)
    }

    canApplyPushDamage() {
        return this.isPushed() && this.hasBanes() && this.isPureFlRoll()
    }

    applyPushDamage(actor) {
        if(!this.canApplyPushDamage())
            return null // TODO: Should not happen; Maybe signal error to user?

        const attributeDamage = {} // key: attribute, value: damage
        const itemDamage = {} // key: item id, value: damage

        // As this roll is a "pure FL roll" all terms are dies with flType
        // No further checks are needed
        for(const term of this.dice) {
            const o = term.options
            switch(o.flType) {
                case 'base':
                    attributeDamage[o.attribute] =
                        (attributeDamage[o.attribute] || 0) +
                        term.banes
                    break
                case 'gear':
                    if(o.item)
                        itemDamage[o.item] =
                            (itemDamage[o.item] || 0) +
                            term.banes
                    break
            }
        }

        actor.applyDamage({
            attributes: attributeDamage,
            items: itemDamage,
            willpower: true
        })
    }
}
FLRoll.FL_CHAT_TEMPLATE = registerTemplate('roll-chat-card')
CONFIG.Dice.rolls.unshift(FLRoll) // Add FLRoll as first entry, so it is used by default
                                  // See Roll.create
