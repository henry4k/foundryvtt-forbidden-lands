import { registerTemplate } from '../utils'
import { constantsData } from '../constants'
import { activateCounterBars } from '../counter-bar'
import { FLRoll } from './core'

export class FLRollDialogModule {
    constructor({dialog, id}) {
        this.dialog = dialog
        this.id = id
    }

    close(options) {}

    /**
     * Returns a list of dice term descriptions, which have the following structure:
     *
     * {
     *   dieType: base|skill|gear|artifact (required)
     *   value: ... (required)
     *   modificator: true|false (optional, defaults to false)
     *   options: {} (optional; defines options of generated DiceTerm; is not merged except for flavor!)
     * }
     */
    getDiceTerms() {
        return []
    }

    update(data) {}

    template() {
        throw 'Implementation needed'
    }

    getData() {
        return {
            template: () => this.template(),
            path: `modules.${this.id}`
        }
    }

    activateListeners(html) {}

    onDrop(data) {
        return false;
    }
}

const rollDialogTemplate = registerTemplate('roll-dialog')
class FLRollDialog extends FormApplication {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            height: 'auto',
            template: rollDialogTemplate,
            classes: ['dialog', 'form', 'forbidden-lands', 'roll-dialog'],
            dragDrop: [{dropSelector: null}],
            submitOnChange: true,
            closeOnSubmit: false
        })
    }

    constructor({actor, modules=[], rollMode, title, completionCallback}, options) {
        const data = {}
        // ...
        super(data, options)
        this.data = data
        this.actor = actor
        this.rollMode = rollMode
        this.modules = modules.map((options, index) => {
            options.dialog = this
            options.id = `${index}`
            return new options.class(options)
        })
        this.modulesById = Object.fromEntries(this.modules.map(m => [m.id, m]))
        this.completionCallback = completionCallback || (() => {})
        this._title = title

        // Register the sheet as an active Application for the Entity
        actor.apps[this.appId] = this
    }

    /** @override */
    async close(options) {
        for(const module of this.modules)
            module.close(options)
        delete this.actor.apps[this.appId]
        return super.close(options)
    }

    /** @override */
    get title() {
        return this._title
    }

    /** @override */
    getData() {
        return {
            modules: Object.fromEntries(
                this.modules.map(module => [module.id, module.getData()])),
            actor: this.actor,
            constants: constantsData,
            rollModes: CONFIG.Dice.rollModes,
            rollMode:  this.rollMode
        }
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html)
        html = html[0] // substitute jQuery-Object for the real HTMLElement

        for(const module of this.modules)
            module.activateListeners(html)

        activateCounterBars(this, html)

        html.querySelector('[data-name=rollButton]').addEventListener('click', event => {
            event.preventDefault()
            this.completionCallback()
        })
    }

    /** @override */
    async _updateObject(event, formData) {
        formData = expandObject(formData)

        for(const [id, data] of Object.entries(formData.modules||{})) {
            const module = this.modulesById[id]
            module.update(data)
        }

        this.rollMode = formData.rollMode

        this.render()
    }

    /** @override */
    _canDragDrop(selector) {
        return true
    }

    /** @override */
    async _onDrop(event) {
        // Try to extract the data
        let data
        try {
            data = JSON.parse(event.dataTransfer.getData('text/plain'))
        } catch (err) {
            return false
        }

        for(const module of this.modules)
            if(module.onDrop(data))
                break
    }

    /** @override */
    setPosition(p={}) {
        // forcefully remove width & height restriction
        p.width  = null
        p.height = null
        p = super.setPosition(p)
        const element = this.element[0]
        element.style.width  = ''
        element.style.height = ''
        return p
    }
}

const mergePool = pool => {
    pool.sort((a, b) => {
        const av = a.value
        const bv = b.value
        if(av < 0 && bv < 0)
            return av - bv
        else
            return bv - av
    })
    // 3 2 1 -3 -2 -1

    const flavorBuffer = []
    let totalValue = 0
    for(const [index, termDesc] of pool.entries()) {
        const value = termDesc.value
        totalValue += value

        const negate = value < 0 && index > 0
        if(index > 0)
            flavorBuffer.push(negate ? ' - ' : ' + ')
        const flavor = termDesc.options.flavor
        const flavorValue = negate ? -value : value
        if(flavor) {
            flavorBuffer.push(flavor)
            flavorBuffer.push(` (${flavorValue})`)
        } else {
            flavorBuffer.push(`${flavorValue}`)
        }
    }

    const merged = mergeObject({options: {}}, pool[0])
    merged.value = totalValue
    merged.options.flavor = flavorBuffer.join('')
    return merged
}

const buildRoll = modules => {
    const dieTypes = ['base', 'skill', 'gear', 'artifact']

    const pools = Object.fromEntries(dieTypes.map(t => [t,[]]))
    for(const module of modules) {
        for(const termDesc of module.getDiceTerms()) {
            pools[termDesc.dieType].push(termDesc)
        }
    }

    const filterEmptyDiceTerms = () => {
        for(const dieType of dieTypes)
            pools[dieType] = pools[dieType].filter(termDesc => termDesc.value != 0)
    }
    filterEmptyDiceTerms()

    const merge = (pool, targetTermDesc, predicate) => {
        const unmergedTerms = []
        const mergedTerms = []
        if(targetTermDesc)
            mergedTerms.push(targetTermDesc)
        for(const termDesc of pool.values()) {
            if(termDesc == targetTermDesc)
                continue
            if(predicate(termDesc))
                mergedTerms.push(termDesc)
            else
                unmergedTerms.push(termDesc)
        }
        return [mergePool(mergedTerms)].concat(unmergedTerms)
    }

    // Merge negative terms:
    for(const dieType of dieTypes) {
        const pool = pools[dieType]

        let positiveMax = 0
        let positiveMaxIndex = 0
        let negativeModifierSum = 0
        for(const [index, termDesc] of pool.entries()) {
            const value = termDesc.value
            if(value > positiveMax) {
                positiveMax = value
                positiveMaxIndex = index
            }
            if(termDesc.modifier && value < 0)
                negativeModifierSum += value
        }

        if(negativeModifierSum == 0)
            continue

        if(-negativeModifierSum < positiveMax) {
            // Merge ALL NEGATIVE modifiers with the largest positive entry
            pools[dieType] =  merge(pool, pool[positiveMaxIndex], termDesc => termDesc.modifier && termDesc.value < 0)
        } else {
            // Merge ALL modifiers with the largest positive entry
            pools[dieType] =  merge(pool, pool[positiveMaxIndex], termDesc => termDesc.modifier)
        }
    }

    filterEmptyDiceTerms()

    const rollParts = []
    for(const dieType of dieTypes) {
        for(const termDesc of pools[dieType]) {
            termDesc.rollPart = rollParts.length
            rollParts.push(termDesc)
        }
    }

    const formula = rollParts.map(termDesc => {
        if(termDesc.dieType == 'artifact')
            return `d${termDesc.value}artifact`
        else
            return `${termDesc.value}d6${termDesc.dieType}`
    }).join(' + ')

    const roll = new FLRoll(formula).evaluate()

    for(const [index, term] of roll.dice.entries()) {
        const termDesc = rollParts[index]
        if(termDesc.options)
            mergeObject(term.options, termDesc.options)
    }

    return roll
}

/**
 * Holding SHIFT, ALT or CTRL skips the dialog window and rolls immediateley.
 *
 * @param {Event|object|null} event   The triggering event which initiated the roll
 * @param {FLActor} actor
 * @param {string} rollMode      A specific roll mode to apply as the default for the resulting roll
 * @param {Arrat} modules
 * @param {string|null} title    The dice roll UI window title
 * @param {Object} applicationOptions Forwarded to Foundry VTTs Dialog implementation
 *
 * @return {Promise}             A Promise which resolves once the roll workflow has completed
 */
export const rollDialog = async ({event,
                                  actor,
                                  rollMode,
                                  modules,
                                  title,
                                  applicationOptions}) => {
    const skipDialog = event && (event.shiftKey ||
                                 event.altKey   ||
                                 event.ctrlKey  ||
                                 event.metaKey)

    const dialogArgs = {
        actor: actor,
        modules: modules,
        rollMode: rollMode || game.settings.get('core', 'rollMode'),
        title: title
    }

    let dialog
    if(skipDialog) {
        dialog = new FLRollDialog(dialogArgs, applicationOptions)
    } else {
        await new Promise(resolve => {
            dialogArgs.completionCallback = resolve
            dialog = new FLRollDialog(dialogArgs, applicationOptions)
            dialog.render(true)
        })
    }

    const roll = buildRoll(dialog.modules)
    rollMode = dialog.rollMode
    dialog.close()

    return {roll: roll, rollMode: rollMode}
}
