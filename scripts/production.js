const debug = true
const logDebug = debug ? console.debug : () => {}

class ProductionStep {
    async setup(stronghold, producers) {
        this.stronghold = stronghold

        this.producersById = Object.fromEntries(producers.map(
            producer => [producer.data._id, producer]))

        const allocationByMaterial = {}
        for(const producer of producers) {
            const producerId = producer.data._id
            const production = producer.data.data.production
            for(const material of production.consumed) {
                let allocation = allocationByMaterial[material]
                if(!allocation) {
                    allocation = {}
                    allocationByMaterial[material] = allocation
                }
                allocation[producerId] = {
                    target: 0,
                    has: 0
                }
            }
        }
        this.allocationByMaterial = allocationByMaterial
        this.updateMaterialDemands()

        const materialNames = Object.keys(Object.fromEntries(producers
            .map(producer => {
                const production = producer.data.data.production
                return [production.consumed, production.produced]
            })
            .flat(2)
            .map(v => [v, true])))
        const materialStorage = Object.fromEntries(materialNames
            .map(name => {
                const item = stronghold.getRawMaterialItem(name)
                const storage = item ? item.data.data.amount : 0
                return [name, storage]
            }))
        this.materialStorage = materialStorage
    }

    log(producerId, message) {
        logDebug(`${this.producersById[producerId].name}: ${message}`)
    }

    totalDemandDeltaByProducer(producerId) {
        const producer = this.producersById[producerId]
        const production = producer.data.data.production
        const materials = production.consumed
        const target = production.amount
        const allocations = materials
            .map(material => this.allocationByMaterial[material][producerId])

        //  Unterschiedliche Eingangsmaterialien aufaddieren um Überschuss zu berechnen
        const hasTotal = allocations
            .map(a => a.has)
            .reduce((sum, value) => sum + value, 0)

        return target - hasTotal
    }

    updateMaterialDemands() {
        logDebug('### UPDATE MATERIAL DEMANDS ###')
        for(const [producerId, producer] of Object.entries(this.producersById)) {
            const production = producer.data.data.production
            const demand = Math.max(0, this.totalDemandDeltaByProducer(producerId))
            for(const material of production.consumed) {
                const a = this.allocationByMaterial[material][producerId]
                a.target = a.has + demand
                this.log(producerId, `${material} demand = has + demand = ${a.has} + ${demand} = ${a.target}`)
            }
        }
    }

    // Verfügbares Material verteilen
    allocateMaterial(material, allocation) {
        let storage = this.materialStorage[material]
        if(storage === 0) {
            logDebug(`no material allocation because ${material} storage = 0`)
            return
        }

        //  Wiederholen bis keine Konsumenten mehr übrig sind oder alles verteilt wurde (siehe anfang)
        while(true) {
            const unsaturatedProducerIds = Object.entries(allocation)
                .filter(([_, value]) => value.has < value.target)
                .sort((a,b) => (b.target-b.has) - (a.target-a.has)) // sort descending
                .map(([id]) => id)
            if(unsaturatedProducerIds.length === 0)
                break

            // Kontingent = floor(Gesammt / Anzahl der übrigen Konsumenten)
            const share = Math.floor(storage / unsaturatedProducerIds.length)

            // Kontingent auf alle übrigen Konsumenten aufteilen
            storage -= share * unsaturatedProducerIds.length

            for(const id of unsaturatedProducerIds) {
                const a = allocation[id]
                a.has += share
                this.log(id, `<= ${share}x ${material} (Share)`)

                // Rest verteilen (siehe sort oben)
                if(storage > 0) {
                    storage--
                    a.has++
                    this.log(id, `<= 1x ${material} (Rest)`)
                }
            }

            // Hier sollte Storage = 0 sein
            if(storage !== 0)
                throw 'Storage wurde nicht komplett verteilt'

            // Überschüssiges kommt zurück auf den Stapel
            for(const id of unsaturatedProducerIds) {
                const a = allocation[id]
                const excess = Math.max(a.has - a.target, 0)
                a.has -= excess
                storage += excess
                if(excess > 0)
                    this.log(id, `=> ${excess}x ${material} (Excess)`)
            }

            if(storage === 0)
                break
        }

        this.materialStorage[material] = storage // Wert zurückschreiben
    }

    allocateAllMaterials() {
        logDebug('### ALLOCATE ALL MATERIALS ###')
        for(const [material, allocation] of Object.entries(this.allocationByMaterial))
            this.allocateMaterial(material, allocation)
    }

    totalDemand(material) {
        return Object.values(this.allocationByMaterial[material])
            .map(value => Math.max(value.target - value.has, 0))
            .reduce((sum, value) => sum + value, 0)
    }

    globalTotalDemand() {
        return Object.keys(this.producersById)
            .map(producerId => Math.max(0, this.totalDemandDeltaByProducer(producerId)))
            .reduce((sum, value) => sum + value, 0)
    }

    handleExcess(producerId) {
        const producer = this.producersById[producerId]
        const production = producer.data.data.production
        const materials = production.consumed
        const allocations = materials
            .map(material => this.allocationByMaterial[material][producerId])

        //  Unterschiedliche Eingangsmaterialien aufaddieren um Überschuss zu berechnen
        const totalExcess = Math.max(0, -this.totalDemandDeltaByProducer(producerId))
        this.log(producerId, `total excess = ${totalExcess}`)

        // Gesammter Restbedarf
        let globalDemandByMaterial = materials
            .map((material, i) => Math.min(this.totalDemand(material), allocations[i].has)) // auf vorhandenes begrenzen
        const totalGlobalDemand = globalDemandByMaterial.reduce((sum, value) => sum + value, 0) || 1 // in case of zero
        if(totalGlobalDemand === 0)
            globalDemandByMaterial = globalDemandByMaterial.map(_ => 1)

        // Überschuss anteilig nach Gesammter Restbedarf zurücklegen
        const shareByMaterial = globalDemandByMaterial
            .map(globalDemand => Math.floor(totalExcess * globalDemand / totalGlobalDemand))
        let remainingExcess = totalExcess - shareByMaterial.reduce((sum, value) => sum + value, 0)
        const remainingExcessMaximumShare = Math.ceil(remainingExcess/materials.length)

        logDebug('global demand by material', globalDemandByMaterial)
        this.log(producerId, `total global demand = ${totalGlobalDemand}`)
        logDebug('share by material', shareByMaterial)
        this.log(producerId, `remaining excess = ${remainingExcess}`)
        this.log(producerId, `remaining excess maximum share = ${remainingExcessMaximumShare}`)

        for(let i = 0; i < materials.length; i++) {
            const material = materials[i]
            const allocation = allocations[i]
            let amount = shareByMaterial[i]
            this.log(producerId, `=> ${amount}x ${material} (Share)`)
            if(remainingExcess > 0 && allocation.has > 0) {
                const a = Math.min(remainingExcess, remainingExcessMaximumShare)
                this.log(producerId, `=> ${a}x ${material} min(remaining excess, remaining excess maximum share) = min(${remainingExcess}, ${remainingExcessMaximumShare})`)
                remainingExcess -= a
                amount          += a
            }
            allocation.has                     -= amount
            this.materialStorage[material] += amount
        }
    }

    handleAllExcess() {
        logDebug('### HANDLE ALL EXCESS ###')
        for(const id of Object.keys(this.producersById))
            this.handleExcess(id)
    }

    createProducts() {
        logDebug('### CREATE PRODUCTS ###')
        for(const [producerId, producer] of Object.entries(this.producersById)) {
            const production = producer.data.data.production
            const hasTotal = production.consumed.length > 0
                ? production.consumed
                    .map(material => this.allocationByMaterial[material][producerId].has)
                    .reduce((sum, value) => sum + value, 0)
                : production.amount // functions without consumed materials always produce the given amount
            if(hasTotal > production.amount)
                throw 'Assigned too much material'
            for(const material of production.produced) {
                this.materialStorage[material] += hasTotal
                this.log(producerId, `=> ${hasTotal}x ${material} (Produced)`)
            }
        }
    }

    applyChanges() {
        logDebug('### APPLY CHANGES ###')
        logDebug('material storage', this.materialStorage)
        this.stronghold.setRawMaterialAmount(this.materialStorage)
    }

    step() {
        for(let i = 0; i < 2; i++) {
            this.allocateAllMaterials()
            this.updateMaterialDemands()
            this.handleAllExcess()
            this.updateMaterialDemands()
        }
        this.createProducts()
        this.applyChanges()
    }
}

export const stepProduction = async (stronghold, producers) => {
    const step = new ProductionStep()
    await step.setup(stronghold, producers)
    step.step()
}
