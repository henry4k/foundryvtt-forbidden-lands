import { modulus } from './utils'
import { CounterBase, required } from './counter-base'

export class CounterItem extends CounterBase {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            minValue: 0,
            maxValue: required,
            wrapAround: true,
            renderFn: required
        })
    }

    /**
     * @param {number} minValue
     * @param {number} maxValue
     * @param {boolean} wrapAround
     * @param {function} renderFn fn(element, value)
     */
    constructor(o) {
        super(o)

        if(o.value < o.minValue || o.value > o.maxValue)
            o.value = o.minValue
    }

    /** @override */
    render() {
        super.render()
        const o = this.options
        o.renderFn(o.element, o, this)
    }

    /** @override */
    update(changes) {
        super.update(changes)
        const o = this.options
        const min = o.minValue
        const max = o.maxValue
        if(o.wrapAround) {
            const range = max - min + 1
            o.value = min + modulus((o.value - min), range)
        } else {
            o.value = Math.max(min, Math.min(max, o.value))
        }
    }
}

export class MappedCounterItem extends CounterItem {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            minValue: required,
            maxValue: required,
            mappedValue: required,
            mappedValues: required
        })
    }

    /**
     * @param {Array} valueRange
     */
    constructor(o) {
        o.minValue = 0
        o.maxValue = o.mappedValues.length - 1
        o.value = o.mappedValues.indexOf(o.mappedValue)
        if(o.value === -1)
            throw new Error('Invalid value')
        super(o)
    }

    /** @override */
    update(changes) {
        super.update(changes)
        const o = this.options
        o.mappedValue = o.mappedValues[o.value]
    }
}

const renderDie = (element, o) => {
    const dieType = o.mappedValue

    //element.innerHtml = ''
    for(const child of element.childNodes.values())
        child.remove()

    const child = document.createElement('i')
    child.classList.add('die', 'gear-pool')
    if(dieType === 0) {
        child.classList.add('d6', 'placeholder', 'crossed-out')
    } else {
        child.classList.add(`d${dieType}`)
        element.title = `${game.i18n.localize('FL.DiceLetter')}${dieType}\n${element.title}`
    }
    element.appendChild(child)
}

export const createDieSelection = (dieType, dieTypes, commitFn, options={}) => {
    return new MappedCounterItem(mergeObject({
        mappedValues: dieTypes,
        mappedValue: dieType,
        renderFn: renderDie,
        commitFn: commitFn
    }, options))
}

export const activateDieSelections = (sheet, html) => {
    for(const element of html.querySelectorAll('.die-selection')) {
        const dataset = element.dataset
        const name = dataset.name
        if(!name)
            continue
        const dieTypes = dataset.dieTypes.split(',').map(s => parseInt(s))
        const object = sheet.object
        const data = object instanceof Entity ? object.data : object
        const dieType = getProperty(data, name)
        createDieSelection(
            dieType,
            dieTypes,
            o => sheet._updateObject(new Event('CounterItemUpdate'), {
                _id: sheet.object._id,
                [name]: o.mappedValue
            }),
            {element: element, editable: sheet.options.editable})
    }
}
