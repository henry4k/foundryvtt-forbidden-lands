import { systemRoot, numberName } from './constants'


// ---- TEMPLATES ----

let templates = new Set()

export const getTemplatePath = name => `${systemRoot}/templates/${name}.handlebars`

export const registerTemplate = function(name) {
    const path = getTemplatePath(name)
    if(templates) {
        templates.add(path)
        return path
    } else {
        throw new Error('Templates must be registered before the init event is fired.')
    }
}

export const modulus = (x,y) => ((x % y) + y) % y

export const random = (min, max) => min+Math.floor(twist.random() * (max-min+1))

Handlebars.registerHelper('assetPath', path => systemRoot+'/'+path)

Handlebars.registerHelper('concat', function() {
    const args = Array.prototype.slice.call(arguments, 0, -1)
    const context = arguments[arguments.length-1]
    const separator = context.hash.separator || ''
    return args.join(separator)
})

Handlebars.registerHelper('array', function() {
    return Array.prototype.slice.call(arguments, 0, -1)
})

//export const hashCode = s => {
//    // Works like Javas String.hashCode
//    let h = 0
//    for(let i = 0; i < s.length; i++)
//        h = Math.imul(31, h) + s.charCodeAt(i) | 0
//    return h
//}

export const hashCode = s => {
    // Javas String.hashCode
    // Source: https://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
    let hash = 0
    for (let i = 0; i < s.length; i++) {
        const chr = s.charCodeAt(i)
        hash  = ((hash << 5) - hash) + chr
        hash |= 0 // Convert to 32bit integer
    }
    return hash
}

export const numberFromHash = (min, max, hashStr) =>
    min + (Math.abs(hashCode(hashStr)) % (max-min+1))

Handlebars.registerHelper('random', (min, max, hash) => {
    if(hash.length > 0)
        return min + (Math.abs(hashCode(hash)) % (max-min+1))
    else
        return random(min, max)
})

Handlebars.registerHelper('range', (min, max, options) => {
    let r = ''
    for(let i = min; i <= max; i++)
        r = r + options.fn()
    return r
})

// Pre-load templates:
Hooks.once('init', () => {
    loadTemplates(Array.from(templates.values()))
    templates = null
})

export const activateClickCallbacks = (target, rootElement) => {
    const clickHandler = event => {
        event.preventDefault()
        const element = event.currentTarget
        const methodName = element.dataset.clickCallback
        target[methodName](event)
    }
    rootElement.querySelectorAll('[data-click-callback]').forEach(element =>
        element.addEventListener('click', clickHandler))
}

export const activateValueButtons = async (sheet, rootElement) => {
    rootElement.querySelectorAll('button[value]').forEach(button =>
        button.addEventListener('click', event => {
            event.preventDefault()
            const button = event.currentTarget
            const dtype = button.dataset.dtype
            const rawValue = button.value
            let value
            switch(dtype) {
                case 'String':
                    value = rawValue
                    break
                case 'Number':
                    value = rawValue !== '' ? Number(rawValue) : null
                    break
                case 'Boolean':
                    value = rawValue === 'true'
                    break
                default:
                    value = JSON.parse(rawValue)
            }
            sheet.entity.update({[button.name]: value})
    }))
}

const selectElementContents = element => {
    if(element.select) {
        element.select()
    } else {
        requestAnimationFrame(() => {
            const range = document.createRange()
            range.selectNodeContents(element)
            const selection = window.getSelection()
            selection.removeAllRanges()
            selection.addRange(range)
        })
    }
}

const setupTextInputElement = (element, onChange) => {
    if(element.contentEditable === 'true') {
        const originalValue = element.textContent
        element.addEventListener('keypress', event => {
            if(event.key === 'Enter') {
                event.preventDefault()
                element.blur()
            }
        }, {capture: true})
        element.addEventListener('blur', _event => {
            const value = element.textContent
            if(value !== originalValue)
                onChange(element, value)
        }, {capture: true})
    } else {
        element.addEventListener('change', event => {
            event.preventDefault()
            event.stopImmediatePropagation()
            onChange(element, element.value)
        }, {capture: true})
    }
}

let numberHint
Hooks.once('setup', function() {
    numberHint = game.i18n.localize('FL.NumberInputHint')
})
export const activateNumberInputs = (sheet, rootElement, entity, selector) => {
    if(!sheet.options.editable)
        return
    if(!entity)
        entity = sheet.entity
    if(!selector)
        selector = 'input[type=text][data-dtype=Number]'
    const focusListener = event => {
        const element = event.currentTarget
        selectElementContents(element)
    }
    const changeListener = (element, newValue) => {
        const name = element.name || element.dataset.name
        let value = getProperty(entity.data, name)
        switch(newValue.charAt(0)) {
            case '+':
                value += Number(newValue.substr(1))
                break
            case '-':
                value -= Number(newValue.substr(1))
                break
            default:
                value = Number(newValue)
        }
        if(typeof(value) === 'number' && value !== NaN)
            entity.update({[name]: value})
    }
    rootElement.querySelectorAll(selector).forEach( element => {
        element.title = numberHint
        element.addEventListener('focus', focusListener)
        setupTextInputElement(element, changeListener)
    })
}

const findEntityInHtml = (sheet, element) => {
    const container = sheet.entity
    const itemElement = element.closest('[data-item-id]')
    if(itemElement)
        return container.getOwnedItem(itemElement.dataset.itemId)
    else
        return container
}

export const activateToggleButtons = (sheet, rootElement) => {
    rootElement.querySelectorAll('.toggle-button').forEach(element =>
        element.addEventListener('click', event => {
            event.preventDefault()
            const element = event.currentTarget
            const name = element.dataset.name
            const entity = findEntityInHtml(sheet, element)
            const value = getProperty(entity.data, name)
            entity.update({[name]: !value})
        }))
}

export const activateInlineLists = (rootElement, selector, addCallback, removeCallback) => {
    rootElement.querySelectorAll(selector).forEach(listElement => {
        listElement.querySelectorAll('a').forEach(entryElement =>
            entryElement.addEventListener('click', event => {
                event.preventDefault()
                const element = event.currentTarget
                const value = element.dataset.value
                removeCallback(value)
            }))

        const inputElement = listElement.querySelector('.new-entry')
        if(inputElement)
            inputElement.addEventListener('change', event => {
                event.preventDefault()
                event.stopImmediatePropagation()
                addCallback(inputElement.value)
            }, {capture: true})
    })
}

export const addEntityListEntry = (entity, listName, value) => {
    let list = getProperty(entity.data, listName) || []
    if(value === undefined ||
       value === null ||
       value === '' ||
       list.includes(value))
        return
    list = cloneArray(list)
    list.push(value)
    entity.update({[listName]: list})
}

export const removeEntityListEntry = (entity, listName, value) => {
    let list = getProperty(entity.data, listName)
    const index = list.indexOf(value)
    if(index === -1)
        return
    list = cloneArray(list)
    list.splice(index, 1)
    entity.update({[listName]: list})
}

/*
const transitionEndEventName = (() => {
    if('WebkitTransition' in document.documentElement.style)
        return 'webkitTransitionEnd'
    if('transitionend' in document.documentElement.style)
        return 'transitionend'
    return null
})()

const transitionRunTimeout = 10 // in ms

export const cssTransition = async (element, cssClass) => {
    if(!transitionEndEventName) {
        element.classList.add(cssClass)
        return
    }
    const transitionRunPromise = Promise.race([
        new Promise(setTimeout(() => resolve(false), transitionRunTimeout)),
        new Promise(resolve =>
            element.addEventListener('transitionrun', () => resolve(true), {once: true, passive: true}))])
    element.classList.add(cssClass)
    if(await transitionRunPromise)
        return await new Promise(resolve =>
            element.addEventListener(transitionEndEventName, resolve, {once: true, passive: true}))
}
*/

const noBreakSpace = '\u00A0'
export const formatUnits = (value, unit) =>
    `${numberName(value)}${noBreakSpace}${value === 1 ? unit.labelSingular : unit.labelPlural}`

export const cloneArray = array => array.slice()
