import { registerTemplate } from './utils'
import { FLActorSheet } from './actor-sheet'
import { itemTypes, dayQuarters } from './constants'

const strongholdSheetTemplate = registerTemplate('stronghold-sheet')

const hirelingDropZoneSelector = '.stronghold-function > *, .stronghold-hireling-grid';

export class FLStrongholdSheet extends FLActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['forbidden-lands', 'sheet', 'stronghold'],
            template: strongholdSheetTemplate
            //width: 600,
            //height: 600,
        })
    }

    /** @override */
    getData() {
        const sheetData = super.getData()
        const actorData = sheetData.actor

        const quarterDay = actorData.data.quarterDay
        sheetData.day = Math.floor(quarterDay / 4)+1
        sheetData.dayQuarter = dayQuarters[quarterDay % 4]

        let reputation = 0
        let defenseRating = 0
        const strongholdFunctions = sheetData.items.filter(
            item => item.type === 'strongholdFunction')
        for(const strongholdFunction of strongholdFunctions) {
            const amount = strongholdFunction.data.amount
            reputation    += amount * strongholdFunction.data.reputation
            defenseRating += amount * strongholdFunction.data.defenseRating
            strongholdFunction.data.production.isProducing =
                this.actor.getOwnedItem(strongholdFunction._id).isProducing(quarterDay)
        }
        actorData.data.reputation = reputation
        actorData.data.defenseRating = defenseRating

        const totalSalary = sheetData.items
            .filter(item => item.type === 'strongholdHireling')
            .reduce((total, hireling) => total + hireling.data.salary, 0)
        actorData.data.totalSalary = totalSalary

        const strongholdItems = sheetData.items
            .filter(item => itemTypes[item.type].isGear)
        sheetData.strongholdItems = strongholdItems

        this._getHirelingAssociationData(sheetData)

        return sheetData
    }

    _getHirelingAssociationData(sheetData) {
        const strongholdFunctionsById = {}
        const strongholdHirelings = []
        for(const item of sheetData.items) {
            if(item.type === 'strongholdFunction') {
                strongholdFunctionsById[item._id] = item
                item.data.associatedHirelings = []
            } else if(item.type === 'strongholdHireling')
                strongholdHirelings.push(item)
        }

        for(const hireling of strongholdHirelings) {
            const associatedFunctionId = hireling.data.associatedFunctionId
            if(associatedFunctionId && associatedFunctionId in strongholdFunctionsById) {
                const strongholdFunction = strongholdFunctionsById[associatedFunctionId]
                strongholdFunction.data.associatedHirelings.push(hireling)
                hireling.data.associatedFunction = strongholdFunction
            }
        }
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html)

        html = html[0] // substitute jQuery-Object for the real HTMLElement

        // Everything below here is only needed if the sheet is editable
        if(!this.options.editable)
            return

        html.querySelector('.item-create-dialog').addEventListener('click',
            event => {
                event.preventDefault()
                this._createNewStrongholdItemDialog()
            })

        html.querySelectorAll('.stronghold-function').forEach(e => this._activateStrongholdFunction(e))

        html.querySelectorAll('a[data-step-size]').forEach(element =>
            element.addEventListener('click', this._onTimeStepClick.bind(this)))

        this._activateHirelingAssociation(html)
    }

    _activateHirelingAssociation(html) {
        for(const element of html.querySelectorAll('.stronghold-hireling .item-name')) {
            element.setAttribute('draggable', true)
            element.addEventListener('dragstart', this._onHirelingDragStart.bind(this))
            element.addEventListener('dragend', this._onHirelingDragEnd.bind(this))
        }

        for(const element of html.querySelectorAll(hirelingDropZoneSelector)) {
            element.addEventListener('drop', this._onHirelingDrop.bind(this))
        }

        for(const element of html.querySelectorAll('.associated-hirelings-list > a, .stronghold-hireling > .association')) {
            element.addEventListener('click', this._onHirelingClearAssociatenClick.bind(this))
        }
    }

    _enableHirelingDropZones(html) {
        for(const element of html.querySelectorAll(hirelingDropZoneSelector)) {
            element.classList.add('dropzone')
        }
    }

    _disableHirelingDropZones(html) {
        for(const element of html.querySelectorAll(hirelingDropZoneSelector)) {
            element.classList.remove('dropzone')
        }
    }

    _onHirelingDragStart(event) {
        event.stopPropagation()
        const itemElement = event.currentTarget.closest('.stronghold-hireling')
        const itemId = itemElement.dataset.itemId
        event.dataTransfer.setData('text/plain', itemId)

        const html = itemElement.closest('form')
        this._enableHirelingDropZones(html)
    }

    _onHirelingDragEnd(event) {
        const itemElement = event.currentTarget.closest('.stronghold-hireling')
        const html = itemElement.closest('form')
        this._disableHirelingDropZones(html)
    }

    _onHirelingDrop(event) {
        event.preventDefault()
        const hirelingItemId = event.dataTransfer.getData('text/plain')
        const hireling = this.actor.getOwnedItem(hirelingItemId)

        if(event.currentTarget.classList.contains('stronghold-hireling-grid')) {
            hireling.clearAssociatedFunctionId()
        } else {
            const itemElement = event.currentTarget.closest('.stronghold-function')
            const itemId = itemElement.dataset.itemId
            hireling.setAssociatedFunctionId(itemId)
        }
    }

    _onHirelingClearAssociatenClick(event) {
        event.preventDefault()
        const itemElement = event.currentTarget.closest('[data-item-id]')
        const hirelingId = itemElement.dataset.itemId
        const hireling = this.actor.getOwnedItem(hirelingId)
        hireling.clearAssociatedFunctionId()
    }

    _activateStrongholdFunction(itemElement) {
        const itemId = itemElement.dataset.itemId
        const item = this.actor.getOwnedItem(itemId)
        for(name of ['step', 'start', 'stop']) {
            const button = itemElement.querySelector(`.stronghold-function-${name}`)
            if(button)
                button.addEventListener('click', this._onStrongholdFunctionButtonClick.bind(this, item, name))
        }
    }

    _onTimeStepClick(event) {
        const element = event.currentTarget
        const stepSize = parseInt(element.dataset.stepSize)
        this.actor.stepContinousProduction(stepSize)
    }

    _onStrongholdFunctionButtonClick(item, buttonName, event) {
        event.preventDefault()
        switch(buttonName) {
            case 'step':
                this.actor.stepProduction([item])
                break
            case 'start':
                item.stopProduction(false)
                break
            case 'stop':
                item.stopProduction(true)
                break
        }
    }

    _createNewStrongholdItemDialog() {
        const create = html => {
            html = html[0] // substitute jQuery-Object for the real HTMLElement
            const type = html.querySelector('input:checked').value
            this._createAndEditItem(type)
        }
        let content = []
        content = content.concat(Object.values(itemTypes)
            .filter(type => type.isGear)
            .sort((a,b) => a.label.localeCompare(b.label))
            .map(type => `<label><input type="radio" name="type" value="${type.name}"${type.name === 'gear' ? ' checked' : ''}>${type.label}</label><br>`))
        const dialog = new Dialog({
            title: game.i18n.localize('ITEM.Create'),
            content: content.join(''),
            buttons: {
                create: {
                    icon: '<i class="fas fa-plus-circle"></i>',
                    label: game.i18n.localize('FL.Create'),
                    callback: create
                },
                cancel: {
                    icon: '<i class="fas fa-times"></i>',
                    label: game.i18n.localize('Cancel')
                }
            },
            default: 'create'
        });
        dialog.render(true);
    }
}
