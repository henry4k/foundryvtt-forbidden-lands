import { registerTemplate } from './utils'
import { attributes as attributeDescriptions, resourceList } from './constants'
import { activateCounterBars } from './counter-bar'
import { activateDieSelections } from './counter-item'
import { FLActorSheet, sortItems } from './actor-sheet'

registerTemplate('attribute')
const characterSheetTemplate = registerTemplate('character-sheet')
const gearGridTemplate = registerTemplate('gear-grid')
Hooks.once('init', async () => {
    Handlebars.registerPartial('gearGrid', await getTemplate(gearGridTemplate))
})

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class FLCharacterSheet extends FLActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['forbidden-lands', 'sheet', 'character'],
            template: characterSheetTemplate,
            //width: 600,
            //height: 600,
            tabs: [{navSelector: 'nav.tabs', contentSelector: '.sheet-body', initial: 'character'}]
        })
    }

    /* -------------------------------------------- */

    /** @override */
    getData() {
        const sheetData = super.getData()
        const actorData = sheetData.actor

        const attributes = actorData.data.attributes
        const skills     = actorData.data.skills
        sheetData.attributes = Object.fromEntries(Object.entries(attributeDescriptions).map(([attributeName, attributeDesc]) => {
            const attribute = attributes[attributeDesc.name]
            return [attributeName, {
                name:  attributeDesc.name,
                label: attributeDesc.label,
                value: attribute.value,
                max:   attribute.max,
                skills: Object.values(attributeDesc.skills).map(skillDesc => {
                    const skill = skills[skillDesc.name]
                    return {
                        name:  skillDesc.name,
                        label: skillDesc.label,
                        value: skill.value
                    }
                })
            }]
        }))

        const resources = actorData.data.resources
        sheetData.resourceList = resourceList.map(resourceDesc => {
            const name = resourceDesc.name
            const resource = resources[name]
            return {
                name: name,
                label: resourceDesc.label,
                imageLineart: resourceDesc.imageLineart,
                imageColor: resourceDesc.imageColor,
                die: resource.die
            }
        })

        sheetData.afflictions = sheetData.items.filter(item =>
            item.type === 'criticalInjury' || item.type === 'disease')
        sortItems(sheetData.afflictions)

        this._getTalentAndSpellData(sheetData)

        return sheetData
    }

    _sortSpellList(spells) {
        sortItems(spells)
        // TODO: Sort by rank AND sort key
    }

    _getSpellCssClass(talentRank, spell) {
        const rankDiff = talentRank - spell.data.rank
        if(rankDiff >= 0)
            return 'known'
        else if(rankDiff === -1)
            return 'chance-cast'
        else
            return 'unknown'
    }

    _getTalentAndSpellData(sheetData) {

        const talents = []
        const spells = []
        for(const item of sheetData.items) {
            if(item.type === 'talent')
                talents.push(item)
            else if(item.type === 'spell')
                spells.push(item)
        }

        const talentsByName = Object.fromEntries(talents.map(
            talent => [talent.name, talent]))

        const magicTalentsById = {}
        const magicTalents = []
        let generalSpells = []
        for(const spell of spells) {
            const talentName = spell.data.talent
            if(talentName && talentName in talentsByName) {
                const talent = talentsByName[talentName]
                let spells = talent.data.spells
                if(!spells) {
                    spells = []
                    talent.data.spells = spells
                    magicTalents.push(talent)
                    magicTalentsById[talent._id] = talent
                }
                spells.push(spell)
            } else
                generalSpells.push(spell)
        }

        const otherTalents = talents.filter(talent => !(talent._id in magicTalentsById))

        for(const talent of magicTalents)
            this._sortSpellList(talent.data.spells)
        this._sortSpellList(generalSpells)

        const maxMagicTalentRank = magicTalents.reduce(
            (max, talent) => Math.max(max, talent.data.rank), 0)

        if(!sheetData.data.ui.hiddenSpellsVisible) {
            generalSpells = generalSpells.filter(spell =>
                spell.data.rank <= maxMagicTalentRank)
            for(const talent of magicTalents) {
                const talentRank = talent.data.rank
                talent.data.spells = talent.data.spells.filter(spell =>
                    spell.data.rank <= talentRank)
            }
        }

        for(const spell of generalSpells)
            spell.data.cssClass = this._getSpellCssClass(maxMagicTalentRank, spell)
        for(const talent of magicTalents) {
            const talentRank = talent.data.rank
            for(const spell of talent.data.spells)
                spell.data.cssClass = this._getSpellCssClass(talentRank, spell)
        }

        sheetData.otherTalents = otherTalents
        sheetData.magicTalents = magicTalents
        sheetData.generalSpells = generalSpells
        sheetData.maxMagicTalentRank = maxMagicTalentRank
    }

    /* -------------------------------------------- */

    /** @override */
    activateListeners(html) {
        super.activateListeners(html)

        html = html[0] // substitute jQuery-Object for the real HTMLElement

        activateCounterBars(this, html)
        activateDieSelections(this, html)
    }

    /** @override */
    _activateItem(itemElement) {
        super._activateItem(itemElement)

        const itemId = itemElement.dataset.itemId
        const item = this.actor.getOwnedItem(itemId)

        if(item.type === 'criticalInjury') {
            const stateClickHandler = event => {
                event.preventDefault()
                item.editCriticalInjuryState(event.type === 'click')
            }
            const stateButton = itemElement.querySelector('.critical-injury-state')
            stateButton.addEventListener('click',       stateClickHandler)
            stateButton.addEventListener('contextmenu', stateClickHandler)

            const lethalButton = itemElement.querySelector('.critical-injury-lethal')
            if(lethalButton)
                lethalButton.addEventListener('click', event => {
                    event.preventDefault()
                    item.setLethal(false)
                })
        } else if(item.type === 'disease') {
            itemElement.querySelector('.sickness-roll').addEventListener('click', event => {
                event.preventDefault()
                item.sicknessRoll()
            })
        }
    }

    async _onDrop(event) {
        // TODO: => item.js!
        const result = await super._onDrop(event)
        if('type' in result && result.type === 'talent')
            this.actor.addSpellsFromWorld(result.name)
        return result
    }
}
