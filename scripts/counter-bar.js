import { CounterBase } from './counter-base'
import { numberFromHash } from './utils'

let maxHint
Hooks.once('setup', function() {
    maxHint = game.i18n.localize('FL.CounterBarModifyMaximumHint')
})

const styleFunctions = {
    simple: (element, value, state, counterBar) => {
        element.classList.add(state)
    },
    attribute: (element, value, state, counterBar) => {
        element.classList.add(state)
        element.classList.add('checkbox'+numberFromHash(1, 24, counterBar.options.id+value))
    },
    gearBonus: (element, value, state, counterBar) => {
        const cl = element.classList
        cl.add('die')
        cl.add('d6')
        cl.add('gear-pool')
        switch(state) {
            case 'off':
                cl.add('placeholder')
                break
            case 'placeholder':
                cl.add('placeholder')
                cl.add('crossed-out')
                break
        }
        element.style.transform = `rotate(${numberFromHash(-3, 3, counterBar.options.id+value)}deg)`
    },
    dice: (element, value, state, counterBar) => {
        const o = counterBar.options
        const cl = element.classList
        cl.add('die')
        cl.add('d6')
        cl.add(`${o.dieType}-pool`)
        switch(state) {
            case 'off':
                cl.add('placeholder')
                break
            case 'placeholder':
                cl.add('placeholder')
                cl.add('crossed-out')
                break
            case 'hidden':
                cl.add('hidden')
                break
        }
        if(state === 'on')
            element.style.transform = `rotate(${numberFromHash(-3, 3, o.id+value)}deg)`
    },
    coin: (element, value, state, counterBar) => {
        //element.style.zIndex = value
        const offset = numberFromHash(-2, 2, counterBar.options.id+value)
        if(offset != 0)
            element.style.transform = `translateX(${offset}px)`
    }
}

const tryParseInt = (value, fallback=undefined) =>
    value ? parseInt(value) : fallback

export class CounterBar extends CounterBase {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            softLimit: null,
            hardLimit: 100,
            styleFn: styleFunctions.simple,
            omit: null,
            placeholder: false
        })
    }

    static createFromElementDataset(element, application) {
        const d = element.dataset

        const o = {
            element: element,
            editable: d.editable !== 'false' && application.options.editable,
            value: tryParseInt(d.value),
            softLimit: tryParseInt(d.softLimit),
            hardLimit: tryParseInt(d.hardLimit),
            styleFn: styleFunctions[d.style || 'simple'],
            omit: d.omit,
            placeholder: d.placeholder === 'true',
            id: d.id
        }

        if(d.name) {
            if(!application instanceof FormApplication)
                throw new Error('Application must be a FormApplication')
            let object = application.object
            if(d.itemId)
                object = object.getOwnedItem(d.itemId)
            const data = object instanceof Entity
                ? object.data
                : object
            const propertyName = d.name
            const property = getProperty(data, propertyName)
            o.id = object._id + propertyName

            const isSimpleValue = typeof(property) !== 'object'
            o.value = isSimpleValue ? property : property.value
            o.softLimit = isSimpleValue || property.max === undefined
                ? undefined
                : property.max

            if(o.editable) {
                o.commitFn = changes => {
                    const data = {_id: object._id}

                    if(isSimpleValue) {
                        data[propertyName] = changes.value
                    } else {
                        data[`${propertyName}.value`] = changes.value
                        data[`${propertyName}.max`]   = changes.softLimit
                    }

                    //object.update(data)
                    application._updateObject(new Event('CounterBarUpdate'), data)
                }
            }
        }

        for(const key in d)
            if(!(key in o))
                o[key] = d[key]

        return new CounterBar(o)
    }

    /**
     * @param {number} softLimit
     * @param {number} hardLimit Limit for value and soft limit
     * @param {function} styleFn fn(entryElement, value, state, counterBar)
     * @param {string} omit Omit all elements that are in or above the given class (off, hidden)
     * @param {boolean} placeholder Show single placeholder element instead of nothing
     */
    constructor(o) {
        super(o)
    }

    get hasSoftLimit() {
        return typeof(this.options.softLimit) === 'number'
    }

    get limit() {
        const o = this.options
        return this.hasSoftLimit
            ? o.softLimit
            : o.hardLimit
    }

    /** @override */
    render() {
        super.render()

        const o = this.options
        const element = o.element

        if(o.editable && o.softLimit)
            element.title += `\n${maxHint}`

        element.innerHTML = ''
        //for(const child of element.childNodes.values())
        //    child.remove()

        let boxCount
        switch(o.omit) {
            case 'hidden':
                boxCount = this.limit
                break

            case 'off':
                boxCount = o.value
                break

            default:
                boxCount = o.hardLimit
        }
        if(boxCount === 0 && o.placeholder)
            boxCount = 1

        const entryElements = []
        for(let i = 1; i <= boxCount; i++)
            entryElements.push(document.createElement('i'))
        this._styleEntries(1, entryElements)
        entryElements.forEach(element.appendChild.bind(element))
    }

    _styleEntries(startValue, entryElements) {
        // on:     <= value
        // off:    >  value     && <= softLimit
        // hidden: >  softLimit && <= hardLimit

        const o = this.options
        const styleFn = o.styleFn

        const value = o.value
        const hasSoftLimit = this.hasSoftLimit
        const softLimit = o.softLimit
        const limit = this.limit

        for(let i = 0; i < entryElements.length; i++) {
            const entryElement = entryElements[i]
            const entryValue = startValue+i

            let state
            if(entryValue <= value)
                state = 'on'
            else if(entryValue <= limit)
                state = 'off'
            else if(value === 0 && (!hasSoftLimit || softLimit === 0))
                state = 'placeholder'
            else
                state = 'hidden'

            styleFn(entryElement, entryValue, state, this)
        }
    }

    /** @override */
    update(changes) {
        super.update(changes)
        const o = this.options
        const limit = this.hasSoftLimit
            ? o.softLimit
            : o.hardLimit
        o.value = Math.min(o.value, limit)
    }

    /** @override */
    _onClick(event) {
        event.preventDefault()
        const o = this.options
        if(event.shiftKey && this.hasSoftLimit) {
            if(event.type === 'click') {
                if(o.softLimit < o.hardLimit)
                    return this.commit({softLimit: o.softLimit+1})
            } else {
                if(o.softLimit > 0)
                    return this.commit({softLimit: o.softLimit-1})
            }
        } else {
            if(event.type === 'click') {
                if(o.value < this.limit)
                    return this.commit({value: o.value+1})
            } else {
                if(o.value > 0)
                    return this.commit({value: o.value-1})
            }
        }
        this.feedback({})
    }
}

export const activateCounterBars = (formApplication, html) => {
    for(const counterBar of html.querySelectorAll('.counter-bar.generated-counter-bar'))
        CounterBar.createFromElementDataset(counterBar, formApplication)
}

const kebabCaseFromCamelCase = s => s.replace(/([A-Z])/g, c => `-${c.toLowerCase()}`)

Handlebars.registerHelper('counterBar', options => {
    const hash = options.hash
    const cssClass = hash.class || ''
    const buffer = ['<span class="counter-bar generated-counter-bar ', cssClass, '"']
    Object.entries(hash)
        .filter(([key, value]) => key !== 'class' && value !== undefined && value !== null)
        .map(([key, value]) => ` data-${kebabCaseFromCamelCase(key)}="${value}"`)
        .forEach(s => buffer.push(s))
    buffer.push('></span>')
    return new Handlebars.SafeString(buffer.join(''))
})
