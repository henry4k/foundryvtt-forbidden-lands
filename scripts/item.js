import { formatUnits, addEntityListEntry, removeEntityListEntry } from './utils'
import { getWeightInfoByName, timePeriods, intervalByValue, talentTypes } from './constants'
import { FLRoll } from './roll/core'
import { createMessageFromRoll } from './roll/dice-so-nice'

export class FLItem extends Item {
    static async findItemsInWorld(predicate, searchCompediums = false) {
        const items = game.items.filter(item => item.visible && predicate(item))
        if(searchCompediums) {
            const canSeePrivatePacks = game.user.isGM
            const packs = game.packs.filter(pack => pack.metadata.entity === 'Item' &&
                                            pack.private ? canSeePrivatePacks : true)
            const packItems = await Promise.all(packs.map(pack => pack.getContent()))
            packItems.flat()
                     .filter(item => item.visible && predicate(item))
                     .forEach(item => items.push(item))
        }
        return items
    }

    /** @override */
    prepareData() {
        super.prepareData()
        const data = this.data.data

        if(data.weight)
            data.weightInfo = getWeightInfoByName(data.weight)

        switch(this.type) {
            case 'talent':
                this._prepareTalent()
                break
            case 'criticalInjury':
                this._prepareCriticalInjury()
                break
            case 'strongholdFunction':
                this._prepareStrongholdFunction()
                break
        }
    }


    /* ---- EQUIPMENT ---- */

    equip(state) {
        const data = this.data.data
        if(data.equipped === undefined)
            return
        this.update({'data.equipped': state})
    }

    addFeature(name) {
        addEntityListEntry(this, 'data.features', name)
    }

    removeFeature(name) {
        removeEntityListEntry(this, 'data.features', name)
    }


    /* ---- TALENT ----*/

    _prepareTalent() {
        const data = this.data.data
        const type = data.type
        const typeDesc = talentTypes[type]
        const actor = this.actor
        data.typeLabel = actor
            ? typeDesc.resolve(actor.data.data)
            : typeDesc.label
    }


    /* ---- CRITICAL INJURY ---- */

    get _criticalInjuryState() {
        const data = this.data.data
        if(data.lethal) {
            if(data.timeLimit.value === null)
                return 'rollTimeLimit'
            else if(data.timeLimit.value !== 0)
                return 'withinTimeLimit'
            else
                return 'pastTimeLimit'
        } else {
            if(data.healingTime.value === null)
                return 'rollHealingTime'
            else if(data.healingTime.value !== 0)
                return 'withinHealingTime'
            else
                return 'pastHealingTime'
        }
    }

    _prepareCriticalInjury() {
        const data = this.data.data
        let unit
        let value
        let time
        let text
        switch(this._criticalInjuryState) {
            case 'rollTimeLimit':
                unit = timePeriods[data.timeLimit.unit]
                text = game.i18n.format('FL.RollLethalTimeLimit', {timePeriod: unit.labelPlural})
                text += ' <i class="fas fa-dice"></i>'
                break
            case 'withinTimeLimit':
            case 'pastTimeLimit':
                unit = timePeriods[data.timeLimit.unit]
                value = data.timeLimit.value
                time = `<u>${formatUnits(value, unit)}</u>`
                text = game.i18n.format('FL.LethalTimeLimitText', {time: time})
                text += ' <i class="fas fa-angle-double-right"></i>'
                break
            case 'rollHealingTime':
                unit = timePeriods.days
                text = game.i18n.format('FL.RollHealingTime', {timePeriod: unit.labelPlural})
                text += ' <i class="fas fa-dice"></i>'
                break
            case 'withinHealingTime':
                unit = timePeriods.days
                value = data.healingTime.value
                time = `<u>${formatUnits(value, unit)}</u>`
                text = game.i18n.format('FL.HealingTimeText', {time: time})
                text += ' <i class="fas fa-angle-double-right"></i>'
                break
            case 'pastHealingTime':
            default:
                text = ''
        }
        data.stateText = text
    }

    async _rollTime(name, flavorKey, timeUnit) {
        const data = getProperty(this.data.data, name)
        const roll = new Roll(data.formula).roll()
        if(!timeUnit)
            timeUnit = timePeriods[data.unit]
        const flavor = game.i18n.format(flavorKey, {timePeriod: timeUnit.labelPlural})
        await createMessageFromRoll(roll, {flavor: `${this.name} — ${flavor}`})
        this.update({[`data.${name}.value`]: roll.total})
    }

    async rollTimeLimit() {
        this._rollTime('timeLimit', 'FL.LethalTimeLimitFlavor')
    }

    resetTimeLimit() {
        this.update({'data.timeLimit.value': null})
    }

    async rollHealingTime() {
        this._rollTime('healingTime', 'FL.HealingTimeFlavor', timePeriods.days)
    }

    resetHealingTime() {
        this.update({'data.healingTime.value': null})
    }

    editCriticalInjuryState(decrement) {
        const state = this._criticalInjuryState
        const data = this.data.data
        const updateValue = name => {
            const modifier = decrement ? -1 : 1
            this.update({[`data.${name}.value`]: Math.max(0, data[name].value+modifier)})
        }
        switch(state) {
            case 'rollTimeLimit':
                this.rollTimeLimit()
                break
            case 'withinTimeLimit':
            case 'pastTimeLimit':
                updateValue('timeLimit')
                break
            case 'rollHealingTime':
                this.rollHealingTime()
                break
            case 'withinHealingTime':
            case 'pastHealingTime':
                updateValue('healingTime')
                break
        }
    }

    setLethal(value) {
        this.update({'data.lethal': value})
    }


    /* ---- DISEASE ---- */

    async sicknessRoll() {
        //const actor = this.actor
        // TODO: endurance roll vs virulence
        const virulenceLabel = game.i18n.localize('FL.Virulence')
        const virulence = this.data.data.virulence
        const roll = new FLRoll(`${virulence}d6skill[${virulenceLabel}]`).roll()
        const flavor = `${this.name} — ${game.i18n.localize('FL.DailySicknessRoll')}`
        await roll.toMessage({flavor: flavor})
    }


    /* ---- TALENT ---- */

    static async _deleteOwnedTalent(actor, itemData) {
        const name = itemData.name
        const spells = actor.items.filter(item => item.type === 'spell')

        const hasOtherMagicTalents = spells.some(spell => {
            const spellTalent = spell.data.data.talent
            return spellTalent !== '' &&
                   spellTalent !== name
        })

        const associatedSpells = hasOtherMagicTalents
            ? spells.filter(spell => spell.data.data.talent === name)
            : spells

        if(associatedSpells.length === 0)
            return

        // TODO: Dialog.prompt
        const result = await new Promise(resolve => Dialog.confirm({
            title: game.i18n.localize('ITEM.Delete'),
            content: game.i18n.format('FL.DeleteSpellsConfirm', {amount: associatedSpells.length}),
            yes: () => resolve(true),
            no:  () => resolve(false)
        }))
        if(!result)
            return

        const deletedSpellIds = associatedSpells.map(spell => spell._id)
        actor.deleteOwnedItem(deletedSpellIds)
    }


    /* ---- STRONGHOLD FUNCTION ---- */

    _prepareStrongholdFunction() {
        const data = this.data.data
        const production = data.production

        const canProduce = this.canProduce()
        production.canProduce = canProduce

        if(!canProduce) {
            production.productionText = ''
            production.intervalText = ''
            return
        }

        const amount   = production.amount
        const consumed = production.consumed
        const produced = production.produced
        const buffer = [`${amount}&MediumSpace;×&MediumSpace;`]
        if(consumed.length > 0) {
            buffer.push(consumed.join('/'))
            buffer.push(' » ')
        }
        buffer.push(produced.join(', '))
        production.productionText = buffer.join('')

        const interval = production.interval
        production.intervalText = interval !== 0
            ? intervalByValue(interval).label
            : ''
    }

    static _deleteOwnedStrongholdFunction(actor, itemData) {
        const id = itemData._id
        const associatedHirelings = actor.items.filter(
            item => item.type === 'strongholdHireling' &&
                    item.data.data.associatedFunctionId === id)
        FLItem.update(associatedHirelings.map(hireling => ({
            _id: hireling.id,
            'data.associatedFunctionId': ''
        })))
    }

    stopProduction(value) {
        this.update({'data.production.stopped': value})
    }

    addConsumedMaterial(name) {
        addEntityListEntry(this, 'data.production.consumed', name)
    }

    removeConsumedMaterial(name) {
        removeEntityListEntry(this, 'data.production.consumed', name)
    }

    addProducedMaterial(name) {
        addEntityListEntry(this, 'data.production.produced', name)
    }

    removeProducedMaterial(name) {
        removeEntityListEntry(this, 'data.production.produced', name)
    }

    canProduce() {
        const production = this.data.data.production
        return production.amount > 0 &&
               production.produced.length > 0
    }

    isProducing(quarterDay) {
        const production = this.data.data.production
        if(production.stopped || !this.canProduce())
            return false
        const interval = production.interval
        if(interval === 0)
            return false
        if(interval <= 4)
            return quarterDay % 4 <= interval-1
        else
            return quarterDay % interval == 0
    }


    /* ---- STRONGHOLD HIRELING ---- */

    clearAssociatedFunctionId() {
        this.update({'data.associatedFunctionId': ''})
    }

    setAssociatedFunctionId(itemId) {
        this.update({'data.associatedFunctionId': itemId})
    }
}

Hooks.on('preDeleteOwnedItem', (actor, itemData) => {
    switch(itemData.type) {
        case 'talent':
            FLItem._deleteOwnedTalent(actor, itemData)
            break
        case 'strongholdFunction':
            FLItem._deleteOwnedStrongholdFunction(actor, itemData)
            break
    }
    return true
})
